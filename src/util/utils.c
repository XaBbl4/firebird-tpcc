#include "stdafx.h"

#include "../tpcc.h"

void current_timestamp(
	datetime_t* ts)
{
	const int base_day = 40587;

	// getting GMT
	time_t t = time(NULL);
//	struct tm* tm = localtime(&t);

	ts->timestamp_date = t / 86400 + 40587;
	ts->timestamp_time = (t % 86400) * ISC_TIME_SECONDS_PRECISION;
}

void zero_timestamp(
	datetime_t* ts)
{
	// getting GMT
	time_t t = time(NULL);

	ts->timestamp_date = 0;
	ts->timestamp_time = 0;
}

void diagnostics(
	Diag* diag,
	const ISC_STATUS* isc_status,
	int SQLCODE)
{
	char* p = diag->message;

	sprintf(p, "SQL CODE = %d", SQLCODE);
	p += strlen(p);
	*p++ = '\n';

	while(fb_interpret(p, MAX_DIAG_MSG_LEN, &isc_status))
	{
		p += strlen(p);
		*p++ = '\n';
	}
	*p = 0;
}
