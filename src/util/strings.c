//File: STRINGS.C
//Microsoft TPC- C Kit Ver.4.21
//Copyright Microsoft, 1996, 1997, 1998, 1999, 2000
//Purpose: Source file for database loader string functions

#include "stdafx.h"

//Includes
#include "../tpcc.h"
#include <string.h>
#include <ctype.h>

#ifndef _WINDOWS
void itoa(int i, char *buf, int bs)
{
	snprintf(buf, bs, "%d", i);
}
#endif

void PaddString(int max, char *name);

//=======================================================================
//
//Function name: MakeAddress
//
//=======================================================================
void MakeAddress(
	char *street_1,
	char *street_2,
	char *city,
	char *state,
	char *zip)
{
#ifdef DEBUG
	printf("[%ld] DBG: Entering MakeAddress()\n",(int)
		GetCurrentThreadId());
#endif
	MakeAlphaString(10, 20, MAX_STREET_LEN, street_1);
	MakeAlphaString(10, 20, MAX_STREET_LEN, street_2);
	MakeAlphaString(10, 20, MAX_CITY_LEN, city);
	MakeAlphaString(2, 2, MAX_STATE_LEN, state);
	MakeZipNumberString(9, 9, MAX_ZIP_LEN, zip);
#ifdef DEBUG
	printf("[%ld] DBG: MakeAddress: street_1: %s, street_2: %s, city: %s, state: %s, zip: %s\n",
	  (int) GetCurrentThreadId(), street_1, street_2, city, state, zip);
#endif
	return;
}

//=======================================================================
//
//Function name: LastName
//
//=======================================================================
void LastName(int num, char *name)
{
	static char *n[] =
	{
		"BAR", "OUGHT", "ABLE", "PRI", "PRES", 
		"ESE", "ANTI", "CALLY", "ATION", "EING"
	};
#ifdef DEBUG
	printf("[%ld] DBG: Entering LastName()\n",(int) GetCurrentThreadId());
#endif
	if ((num >= 0) && (num < 1000))
	{
		strcpy(name, n[(num / 100) % 10]);
		strcat(name, n[(num / 10) % 10]);
		strcat(name, n[(num / 1) % 10]);
		if (strlen(name) < MAX_LAST_NAME_LEN)
		{
			PaddString(MAX_LAST_NAME_LEN, name);
		}
	}
	else
	{
		printf("\nError in LastName()...num <%d> out of range (0,999)\n", num);
		exit(-1);
	}
#ifdef DEBUG
	printf("[%ld] DBG: LastName: num = [%d] ==> [%d][%d][%d]\n", 
		(int) GetCurrentThreadId(), num, num / 100, (num / 10) % 10, num % 10);
	printf("[%ld] DBG: LastName: String = %s\n",
		(int) GetCurrentThreadId(), name);
#endif
	return;
}
//=======================================================================
//
//Function name: MakeAlphaString
//
//=======================================================================
//philipdu 08/13/96 Changed MakeAlphaString to use A- Z, a- z, and 0- 9 in
//accordance with spec see below:
//The spec says:
//4.3.2.2 The notation random a- string [x ..y]
//(respectively, n- string [x ..y]) represents a string of random alphanumeric
//(respectively, numeric) characters of a random length of minimum x, maximum y,
//and mean(y+ x)/2.Alphanumerics are A..Z, a..z, and 0..9.The only other
//requirement is that the character set used "must be able to represent a minimum
//of 128 different characters".We are using 8 - bit chars, so this is a non issue.
//It is completely unreasonable to stuff non- printing chars into the text fields.
//- CLevine 08/13/96
int MakeAlphaString(int x, int y, int z, char *str)
{
	int len;
	int i;
	char cc = 'a';
	static char chArray[] =
		"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static int chArrayMax = 61;
#ifdef DEBUG
	printf("[%ld] DBG: Entering MakeAlphaString()\n",(int)
		GetCurrentThreadId());
#endif
	len= RandomNumber(x, y);
	for(i= 0; i<len; i++)
	{
		cc = chArray[RandomNumber(0, chArrayMax)];
		str[i] = cc;
	}
	if (len < z)
		memset(str + len, ' ', z - len);
	str[len] = 0;
	return len;
}

//=======================================================================
//
//Function name: MakeOriginalAlphaString
//
//=======================================================================
int MakeOriginalAlphaString(int x, int y, int z, char *str, int percent)
{
	int len;
	int val;
	int start;
#ifdef DEBUG
	printf("[%ld] DBG: Entering MakeOriginalAlphaString()\n",(int)
		GetCurrentThreadId());
#endif
	//verify prercentage is valid
	if ((percent < 0) ||(percent > 100))
	{
		printf("MakeOrigianlAlphaString: Invalid percentage: %d\n", percent);
		exit(-1);
	}
	//verify string is at least 8 chars in length
	if((x + y) <= 8)
	{
		printf("MakeOriginalAlphaString: string length must be >= 8\n");
		exit(- 1);
	}
	//Make Alpha String
	len = MakeAlphaString(x, y, z, str);
	val = RandomNumber(1,100);
	if(val <= percent)
	{
		start = RandomNumber(0, len - 8);
		strncpy(str + start, "ORIGINAL", 8);
	}
#ifdef DEBUG
	printf("[%ld] DBG: MakeOriginalAlphaString: : %s\n",
		(int) GetCurrentThreadId(), str);
#endif
	return strlen(str);
}
//=======================================================================
//
//Function name: MakeNumberString
//
//=======================================================================
int MakeNumberString(int x, int y, int z, char *str)
{
	char tmp[16];
	//MakeNumberString is always called MakeZipNumberString(16, 16,	16, string)
	memset(str, '0', 16);
	itoa(RandomNumber(0, 99999999), tmp, 10);
	memcpy(str, tmp, strlen(tmp));
	itoa(RandomNumber(0, 99999999), tmp, 10);
	memcpy(str+ 8, tmp, strlen(tmp));
	str[16] = 0;
	return 16;
}
//=======================================================================
//
//Function name: MakeZipNumberString
//
//=======================================================================
int MakeZipNumberString(int x, int y, int z, char *str)
{
	char tmp[16];
	//MakeZipNumberString is always called MakeZipNumberString(9, 9, 9, string)
	strcpy(str, "000011111");
	itoa(RandomNumber(0, 9999), tmp, 10);
	memcpy(str, tmp, strlen(tmp));
	return 9;
}
//=======================================================================
//
//Function name: InitString
//
//=======================================================================
void InitString(char *str, int len)
{
#ifdef DEBUG
	printf("[%ld] DBG: Entering InitString()\n",(int)
		GetCurrentThreadId());
#endif
	memset(str, ' ', len);
	str[len] = 0;
}
//=======================================================================
//Function name: InitAddress
//
//Description:
//
//=======================================================================
void InitAddress(char *street_1, char *street_2, char *city, char *state,
	char *zip)
{
	memset(street_1, ' ', MAX_STREET_LEN+ 1);
	memset(street_2, ' ', MAX_STREET_LEN+ 1);
	memset(city, ' ', MAX_CITY_LEN+ 1);
	street_1[MAX_STREET_LEN+ 1] = 0;
	street_2[MAX_STREET_LEN+ 1] = 0;
	city[MAX_CITY_LEN+ 1] = 0;
		memset(state, ' ', MAX_STATE_LEN+ 1);
	state[MAX_STATE_LEN + 1] = 0;
	memset(zip, ' ', MAX_ZIP_LEN+ 1);
	zip[MAX_ZIP_LEN + 1] = 0;
}
//=======================================================================
//
//Function name: PaddString
//
//=======================================================================
void PaddString(int max, char *name)
{
	int len;
	len = strlen(name);
	if(len <max )
		memset(name+ len, ' ', max - len);
	name[max] = 0;
	return;
}
