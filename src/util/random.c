// File: RANDOM.C
// Microsoft TPC- C Kit Ver.4.21
// Copyright Microsoft, 1996, 1997, 1998, 1999, 2000
// Purpose: Random number generation routines for database loader

// Includes
#include "stdafx.h"
#include "../tpcc.h"

// Defines
#define A 16807
#define M 2147483647
#define Q 127773 /* M div A */
#define R 2836 /* M mod A */

#ifdef __GNUC__
# define Thread __thread
#elif __STDC_VERSION__ >= 201112L
# define Thread _Thread_local
#elif defined(_MSC_VER)
# define Thread __declspec(thread)
#else
# error Cannot define Thread
#endif

// Globals
long Thread Seed = 0; /* thread local seed */

/*************************************************************************
* *
* random - *
* Implements a GOOD pseudo random number generator.This generator
*
* will/ should? run the complete period before repeating.*
* *
* Copied from: *
* Random Numbers Generators: Good Ones Are Hard to Find.*
* Communications of the ACM - October 1988 Volume 31 Number 10 *
* *
* Machine Dependencies: *
* long must be 2 ^ 31 - 1 or greater.*
* *
*************************************************************************/

/*************************************************************************
* seed - load the Seed value used in irand and drand.Should be used before *
* first call to irand or drand.*
*************************************************************************/
void seed( long val)
{
#ifdef DEBUG
	printf("[% ld] DBG: Entering seed()...\ n", (int) GetCurrentThreadId());
	printf(" Old Seed %ld New Seed %ld\ n", Seed, val);
#endif
	if ( val <0 )
		val = abs( val);
	Seed = val;
}

/*************************************************************************
* *
* irand - returns a 32 bit integer pseudo random number with a period of *
* 1 to 2 ^ 32 - 1.*
* *
* parameters: *
* none.*
* *
* returns: *
* 32 bit integer - defined as long ( see above ).*
* *
* side effects: *
* seed get recomputed.*
*************************************************************************/
long irand()
{
	register long s; /* copy of seed */
	register long test; /* test flag */
	register long hi; /* tmp value for speed */
	register long lo; /* tmp value for speed */
#ifdef DEBUG
	printf("[% ld] DBG: Entering irand()...\ n", (int) GetCurrentThreadId());
#endif
	s = Seed;
	hi = s / Q;
	lo = s % Q;
	test = A * lo - R * hi;
	if ( test > 0 )
		Seed = test;
	else
		Seed = test + M;
	return( Seed );
}

/*************************************************************************
* *
* drand - returns a double pseudo random number between 0.0 and 1.0.*
* See irand.*
*************************************************************************/
double drand()
{
#ifdef DEBUG
	printf("[% ld] DBG: Entering drand()...\ n", (int) GetCurrentThreadId());
#endif
	return( (double) irand() / 2147483647.0);
}

//=======================================================================
// Function : RandomNumber
//
// Description:
//=======================================================================
long RandomNumber( long lower, long upper)
{
	long rand_num;
#ifdef DEBUG
	printf("[% ld] DBG: Entering RandomNumber()...\ n", (int)
		GetCurrentThreadId());
#endif
	if ( upper == lower ) /* pgd 08-13-96 perf enhancement */
		return lower;
	upper++;
	if ( upper <= lower )
		rand_num = upper;
	else
	rand_num = lower + irand() % (upper - lower); /* pgd 08-13-96 perf enhancement */
#ifdef DEBUG
	printf("[% ld] DBG: RandomNumber between %ld &%ld ==> %ld\ n",.4500 5204- 000 TPC- C Full Disclosure Report B- 29
		(int) GetCurrentThreadId(), lower, upper,
		rand_num);
#endif
	return rand_num;
}

#if 0
// Orginal code pgd 08/ 13/ 96
long RandomNumber( long lower,
				  long upper)
{
	long rand_num;
#ifdef DEBUG
	printf("[% ld] DBG: Entering RandomNumber()...\ n", (int)
		GetCurrentThreadId());
#endif
	upper++;
	if (( upper <= lower))
		rand_num = upper;
	else
		rand_num = lower + irand() % (( upper > lower) ? upper - lower : upper);
#ifdef DEBUG
	printf("[% ld] DBG: RandomNumber between %ld &%ld ==> %ld\ n",
		(int) GetCurrentThreadId(), lower, upper, rand_num);
#endif
	return rand_num;
}
#endif

//=======================================================================
// Function : NURand
//
// Description:
//=======================================================================
long NURand(int iConst, long x, long y, long C)
{
	long rand_num;
#ifdef DEBUG
	printf("[% ld] DBG: Entering NURand()...\ n", (int)
		GetCurrentThreadId());
#endif
	rand_num = ((( RandomNumber( 0, iConst) | RandomNumber( x, y)) + C) % (y-
		x+ 1))+ x;
#ifdef DEBUG
	printf("[% ld] DBG: NURand: num = %d\ n", (int) GetCurrentThreadId(),
		rand_num);
#endif
	return rand_num;
}
