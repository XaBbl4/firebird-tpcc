// File: TIME.C
// Microsoft TPC-C Kit Ver.4.21
// Copyright Microsoft, 1996, 1997, 1998, 1999, 2000
// Purpose: Source file for time functions

// Includes
#include "stdafx.h"

#include "../tpcc.h"

//=======================================================================
//
// Function name: TimeNow
//
//=======================================================================

static UINT64 TimeStart = 0;

UINT64 GetTime()
{
	struct timeb tp;
	ftime(&tp);
	return (((UINT64) tp.time * 1000) + (UINT64) tp.millitm);
}

void TimeInit()
{
	TimeStart = GetTime();
}

ISC_ULONG TimeNow()
{
	UINT64 i64Temp;

#ifdef DEBUG
	printf("[%ld] DBG: Entering TimeNow()\n", (int) GetCurrentThreadId());
#endif

	i64Temp = GetTime() - TimeStart;

	return (ISC_ULONG) i64Temp;
}
