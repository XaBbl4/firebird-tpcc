// File: GETARGS.C
// Microsoft TPC-C Kit Ver.4.21
// Copyright Microsoft, 1996, 1997, 1998, 1999, 2000
// Purpose: Source file for command line processing

#include "stdafx.h"

// Includes
#include "tpcc_load.h"

//=======================================================================
//
// Function name: GetArgsLoader
//
//=======================================================================
void GetArgsLoader(int argc, char ** argv, TPCCLDR_ARGS *pargs)
{
	int i;
	char *ptr;
#ifdef DEBUG
	printf("[% ld] DBG: Entering GetArgsLoader()\n", (int)
		GetCurrentThreadId());
#endif
	/* init args struct with some useful values */
	pargs->server = SERVER;
	pargs->user = USER;
	pargs->password = PASSWORD;
	pargs->database = DATABASE;
	pargs->batch = BATCH;
	pargs->num_warehouses = UNDEF;
	pargs->tables_all = TRUE;
	pargs->table_item = FALSE;
	pargs->table_warehouse = FALSE;
	pargs->table_customer = FALSE;
	pargs->table_orders = FALSE;
//	pargs->loader_res_file = LOADER_RES_FILE;
//	pargs->pack_size = DEFLDPACKSIZE;
	pargs->starting_warehouse = DEF_STARTING_WAREHOUSE;
//	pargs->build_index = BUILD_INDEX;
//	pargs->index_order = INDEX_ORDER;
//	pargs->index_script_path = INDEX_SCRIPT_PATH;
	pargs->scale_down = SCALE_DOWN;
	/* check for zero command line args */
	if (argc == 1 )
		GetArgsLoaderUsage();
	for (i = 1; i <argc; ++i)
	{
		if (argv[i][0] != '-' &&argv[i][0] != '/')
		{
			printf("\nUnrecognized command");
			GetArgsLoaderUsage();
			exit(1);
		}
		ptr = argv[i];
		switch (ptr[1])
		{
			case 'h': /* Fall throught */
			case 'H':
				GetArgsLoaderUsage();
				break;
			case 'D':
				pargs->database = ptr+2;
				break;
			case 'P':
				pargs->password = ptr+2;
				break;
			case 'S':
				pargs->server = ptr+2;
				break;
			case 'U':
				pargs->user = ptr+2;
				break;
			case 'b':
				pargs->batch = atol(ptr+2);
				break;
			case 'W':
				pargs->num_warehouses = atol(ptr+2);
				break;
			case 's':
				pargs->starting_warehouse = atol(ptr+2);
				break;
			case 't':
			{
				pargs->tables_all = FALSE;
				if (strcmp(ptr+2,"item") == 0)
					pargs->table_item = TRUE;
				else 
				if (strcmp(ptr+2,"warehouse") == 0)
					pargs->table_warehouse = TRUE;
				else 
				if (strcmp(ptr+2,"customer") == 0)
					pargs->table_customer = TRUE;
				else 
				if (strcmp(ptr+2,"orders") == 0)
					pargs->table_orders = TRUE;
				else
				{
					printf("\nUnrecognized command");
					GetArgsLoaderUsage();
					exit(1);
				}
				break;
			}
			case 'f':
				pargs->loader_res_file = ptr+2;
				break;
			case 'p':
				pargs->pack_size = atol(ptr+2);
				break;
			case 'i':
				pargs->build_index = atol(ptr+2);
				break;
			case 'o':
				pargs->index_order = atol(ptr+2);
				break;
			case 'c':
				pargs->scale_down = atol(ptr+2);
				break;
			case 'd':
				pargs->index_script_path = ptr+2;
				break;
		default:
			GetArgsLoaderUsage();
			exit(-1);
			break;
		}
	}

	/* check for required args */
	if (pargs->num_warehouses == UNDEF )
	{
		printf("Number of Warehouses is required\n");
		exit(-2);
	}
	return;
}

//=======================================================================
//
// Function name: GetArgsLoaderUsage
//
//=======================================================================
void GetArgsLoaderUsage()
{
#ifdef DEBUG
	printf("[% ld] DBG: Entering GetArgsLoaderUsage()\n", (int)
		GetCurrentThreadId());
#endif
	printf("TPCCLDR:\n\n");
	printf("Parameter Default\n");
	printf("--------------------------------------------------------------\n");
	printf("-W Number of Warehouses to Load Required\n");
	printf("-S Server %s\n", SERVER);
	printf("-U Username %s\n", USER);
	printf("-P Password %s\n", PASSWORD);
	printf("-D Database %s\n", DATABASE);
	printf("-b Batch Size %ld\n", (long) BATCH);
//	printf("-p TDS packet size %ld\n", (long) DEFLDPACKSIZE);
//	printf("-f Loader Results Output Filename %s\n", LOADER_RES_FILE);
	printf("-s Starting Warehouse %ld\n", (long) DEF_STARTING_WAREHOUSE);
//	printf("-i Build Option (data = 0, data and index = 1) %ld\n", (long) BUILD_INDEX);
//	printf("-o Cluster Index Build Order (before = 1, after = 0) %ld\n", (long) INDEX_ORDER);
	printf("-c Build Scaled Database (normal = 0, tiny = 1) %ld\n", (long) SCALE_DOWN);
//	printf("-d Index Script Path %s\n", INDEX_SCRIPT_PATH);
	printf("-t Table to Load all tables \n");
	printf("[item| warehouse| customer| orders]\n");
	printf("Notes: \n");
	printf("-the '-t' parameter may be included multiple times to\n");
	printf("specify multiple tables to be loaded \n");
	printf("-'item' loads ITEM table \n");
	printf("-'warehouse' loads WAREHOUSE, DISTRICT, and STOCK tables\n");
	printf("-'customer' loads CUSTOMER and HISTORY tables \n");
	printf("-'orders' load NEW-ORDER, ORDERS, ORDER-LINE tables\n");
	printf("\nNote: Command line switches are case sensitive.\n");
	exit(0);
}
