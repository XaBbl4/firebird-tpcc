#include "stdafx.h"

#include "tpcc.h"
#include "load/tpcc_load.h"

EXEC SQL
	SET DATABASE db = COMPILETIME "${DATABASE}";

// WINDOWS types for POSIX
#ifndef _WINDOWS

typedef int DWORD;
typedef pthread_t HANDLE;

HANDLE CreateThread(void*, int, void *(*start_routine)(void*), void *arg, int, void*)
{
    pthread_t thread;
    int state;

    if (state = pthread_create(&thread, NULL, start_routine, arg))
        throw "pthread_create";

    return thread;
}

#endif

// Defines

#define MILLI 1000

#define MAXITEMS 100000
#define MAXITEMS_SCALE_DOWN 100
#define CUSTOMERS_PER_DISTRICT 3000
#define CUSTOMERS_SCALE_DOWN 30
#define DISTRICT_PER_WAREHOUSE 10
#define ORDERS_PER_DISTRICT 3000
#define ORDERS_SCALE_DOWN 30
#define MAX_CUSTOMER_THREADS 2
#define MAX_ORDER_THREADS 3
#define MAX_MAIN_THREADS 4

// Shared memory structures

typedef struct
{
	short w_id;
	char w_name[MAX_WAREHOUSE_NAME_LEN + 1];
	char w_street_1[MAX_STREET_LEN + 1];
	char w_street_2[MAX_STREET_LEN + 1];
	char w_city[MAX_CITY_LEN + 1];
	char w_state[MAX_STATE_LEN + 1];
	char w_zip[MAX_ZIP_LEN + 1];
	long w_tax;
	long w_ytd;
} WAREHOUSE_STRUCT;

typedef struct
{
	short d_id;
	short d_w_id;
	char d_name[MAX_DISTRICT_NAME_LEN + 1];
	char d_street_1[MAX_STREET_LEN + 1];
	char d_street_2[MAX_STREET_LEN + 1];
	char d_city[MAX_CITY_LEN + 1];
	char d_state[MAX_STATE_LEN + 1];
	char d_zip[MAX_ZIP_LEN + 1];
	long d_tax;
	long d_ytd;
	long d_next_o_id;
} DISTRICT_STRUCT;

typedef struct
{
	long s_i_id;
	short s_w_id;
	short s_quantity;
	char s_dist_01[MAX_DIST_LEN + 1];
	char s_dist_02[MAX_DIST_LEN + 1];
	char s_dist_03[MAX_DIST_LEN + 1];
	char s_dist_04[MAX_DIST_LEN + 1];
	char s_dist_05[MAX_DIST_LEN + 1];
	char s_dist_06[MAX_DIST_LEN + 1];
	char s_dist_07[MAX_DIST_LEN + 1];
	char s_dist_08[MAX_DIST_LEN + 1];
	char s_dist_09[MAX_DIST_LEN + 1];
	char s_dist_10[MAX_DIST_LEN + 1];
	long s_ytd;
	short s_order_cnt;
	short s_remote_cnt;
	char s_data[MAX_STOCK_DATA_LEN+ 1];
} STOCK_STRUCT;

typedef struct
{
	long i_id;
	long i_im_id;
	char i_name[MAX_ITEM_NAME_LEN + 1];
	long i_price;
	char i_data[MAX_ITEM_DATA_LEN + 1];
} ITEM_STRUCT;

typedef struct
{
	long ol_number;
	long ol_i_id;
	short ol_supply_w_id;
	short ol_quantity;
	long ol_amount;
	char ol_dist_info[MAX_DIST_LEN + 1];
	datetime_t ol_delivery_d;
} ORDER_LINE_STRUCT;

typedef struct
{
	long o_id;
	short o_d_id;
	short o_w_id;
	long o_c_id;
	short o_carrier_id;
	short o_ol_cnt;
	short o_all_local;
	ORDER_LINE_STRUCT o_ol[15];
} ORDERS_STRUCT;

typedef struct
{
	long c_id;
	short c_d_id;
	short c_w_id;
	char c_first[MAX_FIRST_NAME_LEN + 1];
	char c_middle[MAX_MIDDLE_NAME_LEN + 1];
	char c_last[MAX_LAST_NAME_LEN + 1];
	char c_street_1[MAX_STREET_LEN + 1];
	char c_street_2[MAX_STREET_LEN + 1];
	char c_city[MAX_CITY_LEN + 1];
	char c_state[MAX_STATE_LEN + 1];
	char c_zip[MAX_ZIP_LEN + 1];
	char c_phone[MAX_PHONE_LEN + 1];
	char c_credit[CREDIT_LEN + 1];
	long c_credit_lim;
	long c_discount;
	long c_balance;
	long c_ytd_payment;
	short c_payment_cnt;
	short c_delivery_cnt;
	char c_data[MAX_CUSTOMER_DATA_LEN + 1];
	datetime_t c_since;
	long h_amount;
	char h_data[MAX_HISTORY_DATA_LEN + 1];
} CUSTOMER_STRUCT;

typedef struct
{
	char c_last[MAX_LAST_NAME_LEN+ 1];
	char c_first[MAX_FIRST_NAME_LEN+ 1];
	long c_id;
} CUSTOMER_SORT_STRUCT;

typedef struct
{
	long time_start;
} LOADER_TIME_STRUCT;


void CheckForCommit(isc_tr_handle* tr_handle,
    int rows_loaded,
    const char *table_name,
    ISC_ULONG *time_start);


bool_t Warehouse(
	isc_db_handle db,
	isc_tr_handle tr);

bool_t District(
	isc_db_handle db,
	isc_tr_handle tr);

bool_t Stock(
	isc_db_handle db,
	isc_tr_handle tr);

bool_t Item(
	isc_db_handle db,
	isc_tr_handle tr);

bool_t Customer(
	isc_db_handle db,
	isc_tr_handle tr);

bool_t Orders(
	isc_db_handle db,
	isc_tr_handle tr);

void GetPermutation(int *perm, int n);

// Global variables
char szLastError[300];

ORDERS_STRUCT orders_buf[ORDERS_PER_DISTRICT];
CUSTOMER_STRUCT customer_buf[CUSTOMERS_PER_DISTRICT];
long main_time_start;
long main_time_end;
long max_items;
long customers_per_district;
long orders_per_district;
long first_new_order;
long last_new_order;
TPCCLDR_ARGS *aptr, args;

//=======================================================================
//
// Function name: LoadItem
//
//=======================================================================

bool_t check_status(
	ISC_STATUS* isc_status)
{
	bool_t ok;

	ok = (isc_status[1] == 0);

	if (!ok)
		isc_print_status(isc_status);

	return ok;
}

#define ISC_STATUS_SUCCEEDED(status) ((status)[1] == 0)

typedef bool_t (*LoadProc) (
	isc_db_handle db,
	isc_tr_handle tr);

typedef struct
{
	LoadProc loader;
	const char* name;
} LoadData;

bool_t Load(
	const LoadData* load_data);

#ifdef _WINDOWS
DWORD WINAPI LoadThread(void *arg)
#else
void* LoadThread(void *arg)
#endif
{
    const LoadData* load = (const LoadData*) arg;
	Load(load);
	return 0;
}

CRITICAL_SECTION isc_attach_cs;

bool_t Load(
	const LoadData* load_data)
{
	bool_t ok = FALSE;

	ISC_STATUS isc_status[20];
	isc_db_handle db = 0;
	isc_tr_handle tr = 0;

	char dpb[1024];
	int index = 0;
	memset(dpb, 0, 1024);

	dpb[index++] = 1;
	if (size_t len = strlen(aptr->user))
	{
		dpb[index++] = isc_dpb_user_name;
		dpb[index++] = len;
		memcpy(dpb + index, aptr->user, len);
		index += len;
	}
	if (size_t len = strlen(aptr->password))
	{
		dpb[index++] = isc_dpb_password;
		dpb[index++] = len;
		memcpy(dpb + index, aptr->password, len);
		index += len;
	}

	EnterCriticalSection(&isc_attach_cs);
	isc_attach_database(
		isc_status,
		0, aptr->database,
		&db,
		index, dpb);
	LeaveCriticalSection(&isc_attach_cs);

	if (check_status(isc_status))
	{
		EXEC SQL
			SET TRANSACTION NAME tr READ COMMITTED USING db;

		if (check_status(isc_status))
		{
			ok = load_data->loader(db, tr);
			
			if (ok)
				isc_commit_transaction(isc_status, &tr);
			else
				isc_rollback_transaction(isc_status, &tr);

			check_status(isc_status);
		}

		EnterCriticalSection(&isc_attach_cs);
		isc_detach_database(isc_status, &db);
		LeaveCriticalSection(&isc_attach_cs);
		check_status(isc_status);
	}

	return ok;
}

bool_t Item(
	isc_db_handle db,
	isc_tr_handle tr)
{
	ISC_STATUS isc_status[20];
	ITEM_STRUCT _item, *item = &_item;
    ISC_ULONG time_start;
    ISC_ULONG rows_loaded;

	// Seed with unique number
	seed(1);
	printf("Loading item table...\n");

	InitString(item->i_name, MAX_ITEM_NAME_LEN + 1);
	InitString(item->i_data, MAX_ITEM_DATA_LEN + 1);

	time_start = TimeNow();
	rows_loaded = 0;
	for (item->i_id = 1; item->i_id <= max_items; item->i_id++)
	{
		item->i_im_id = RandomNumber(1L, 10000L);
		MakeAlphaString(14, 24, MAX_ITEM_NAME_LEN, item->i_name);
		item->i_price = RandomNumber(100L, 10000L);
		MakeOriginalAlphaString(26, 50, MAX_ITEM_DATA_LEN, item->i_data, 10);

		EXEC SQL
			INSERT TRANSACTION tr INTO db.item (i_id, i_im_id, i_name, i_price, i_data) 
			VALUES (:item->i_id, :item->i_im_id, :item->i_name, :item->i_price, :item->i_data);

		if (!check_status(isc_status))
			break;

		CheckForCommit(&tr, ++rows_loaded, "item", &time_start);
	}

	printf("Finished loading item table.\n");

	return ISC_STATUS_SUCCEEDED(isc_status);
}

//========================================================================
//
// Function : LoadWarehouse
//
// Loads WAREHOUSE table and loads Stock and District as Warehouses are created
//
//========================================================================

bool_t Warehouse (
	isc_db_handle db,
	isc_tr_handle tr)
{
	ISC_STATUS isc_status[20];
	WAREHOUSE_STRUCT _warehouse, *w = &_warehouse;
	ISC_ULONG time_start;
	unsigned int rows_loaded;

	// Seed with unique number
	seed(2);
	printf("Loading warehouse table...\n");

	InitString(w->w_name, MAX_WAREHOUSE_NAME_LEN + 1);
	InitAddress(w->w_street_1, w->w_street_2, w->w_city, w->w_state, w->w_zip);

	time_start = TimeNow();
	rows_loaded = 0;
	for (w->w_id = aptr->starting_warehouse; w->w_id <= aptr->num_warehouses; w->w_id++)
	{
		MakeAlphaString(6, 10, MAX_WAREHOUSE_NAME_LEN, w->w_name);
		MakeAddress(w->w_street_1, w->w_street_2, w->w_city, w->w_state, w->w_zip);
		w->w_tax = RandomNumber(0L, 2000L);
		w->w_ytd = 300000 * MONEY_SCALE;

		EXEC SQL 
			INSERT TRANSACTION tr INTO db.warehouse (w_id, w_name, w_street_1, w_street_2, 
				w_city, w_state, w_zip, w_tax, w_ytd) 
			VALUES (:w->w_id, :w->w_name, :w->w_street_1, :w->w_street_2, 
				:w->w_city, :w->w_state, :w->w_zip, :w->w_tax, :w->w_ytd);
		
		if (!check_status(isc_status))
			break;

		CheckForCommit(&tr, ++rows_loaded, "warehouse", &time_start);
	}

	printf("Finished loading warehouse table.\n");

	Stock(db, tr);
	District(db, tr);

	return ISC_STATUS_SUCCEEDED(isc_status);
}

//=======================================================================
//
// Function : District
//
//=======================================================================
bool_t District(
	isc_db_handle db,
	isc_tr_handle tr)
{
	ISC_STATUS isc_status[20];
	DISTRICT_STRUCT _d, *d = &_d;
    ISC_ULONG time_start;
    ISC_ULONG rows_loaded;

	// Seed with unique number
	seed(4);
	printf("Loading district table...\n");

	InitString(d->d_name, MAX_DISTRICT_NAME_LEN + 1);
	InitAddress(d->d_street_1, d->d_street_2, d->d_city, d->d_state, d->d_zip);

	d->d_ytd = 30000 * MONEY_SCALE;
	d->d_next_o_id = orders_per_district + 1;
	time_start = TimeNow();
	rows_loaded = 0;
	for (d->d_w_id = aptr->starting_warehouse; d->d_w_id <= aptr->num_warehouses; d->d_w_id++)
	{
		for (d->d_id = 1; d->d_id <= DISTRICT_PER_WAREHOUSE; d->d_id++)
		{
			MakeAlphaString(6, 10, MAX_DISTRICT_NAME_LEN, d->d_name);
			MakeAddress(d->d_street_1, d->d_street_2, d->d_city,
				d->d_state, d->d_zip);
			d->d_tax = RandomNumber(0L, 2000L);

			EXEC SQL 
				INSERT TRANSACTION tr INTO db.district (d_id, d_w_id, d_name, d_street_1, d_street_2, 
					d_city, d_state, d_zip, d_tax, d_ytd, d_next_o_id)
				VALUES (:d->d_id, :d->d_w_id, :d->d_name, :d->d_street_1, :d->d_street_2, 
					:d->d_city, :d->d_state, :d->d_zip, :d->d_tax, :d->d_ytd, :d->d_next_o_id);

			if (!check_status(isc_status))
				break;

			CheckForCommit(&tr, ++rows_loaded, "district", &time_start);
		}
	}

	printf("Finished loading district table.\n");

	return ISC_STATUS_SUCCEEDED(isc_status);
}

//=======================================================================
//
// Function : Stock
//
//=======================================================================
bool_t Stock(
	isc_db_handle db,
	isc_tr_handle tr)
{
	ISC_STATUS isc_status[20];
	STOCK_STRUCT _s, *s = &_s;
    ISC_ULONG time_start;
    ISC_ULONG rows_loaded;

	// Seed with unique number
	seed(3);
	printf("Loading stock table...\n");

	s->s_ytd = s->s_order_cnt = s->s_remote_cnt = 0;
	time_start = TimeNow();
	rows_loaded = 0;
	for (s->s_i_id = 1; s->s_i_id <= max_items; s->s_i_id++)
	{
		for (s->s_w_id = 
			aptr->starting_warehouse; s->s_w_id <= aptr->num_warehouses; s->s_w_id++)
		{
			s->s_quantity = (short) RandomNumber(10L, 100L);
			MakeAlphaString(24, 24, MAX_DIST_LEN, s->s_dist_01);
			MakeAlphaString(24, 24, MAX_DIST_LEN, s->s_dist_02);
			MakeAlphaString(24, 24, MAX_DIST_LEN, s->s_dist_03);
			MakeAlphaString(24, 24, MAX_DIST_LEN, s->s_dist_04);
			MakeAlphaString(24, 24, MAX_DIST_LEN, s->s_dist_05);
			MakeAlphaString(24, 24, MAX_DIST_LEN, s->s_dist_06);
			MakeAlphaString(24, 24, MAX_DIST_LEN, s->s_dist_07);
			MakeAlphaString(24, 24, MAX_DIST_LEN, s->s_dist_08);
			MakeAlphaString(24, 24, MAX_DIST_LEN, s->s_dist_09);
			MakeAlphaString(24, 24, MAX_DIST_LEN, s->s_dist_10);
			MakeOriginalAlphaString(26,50, MAX_STOCK_DATA_LEN, s->s_data, 10);
			
			EXEC SQL
				INSERT TRANSACTION tr INTO db.stock (s_i_id, s_w_id, s_quantity, s_dist_01, s_dist_02, s_dist_03, 
					s_dist_04, s_dist_05, s_dist_06, s_dist_07, s_dist_08, s_dist_09, 
					s_dist_10, s_ytd, s_order_cnt, s_remote_cnt, s_data)
				VALUES (:s->s_i_id, :s->s_w_id, :s->s_quantity, :s->s_dist_01, :s->s_dist_02, :s->s_dist_03, 
					:s->s_dist_04, :s->s_dist_05, :s->s_dist_06, :s->s_dist_07, :s->s_dist_08, :s->s_dist_09, 
					:s->s_dist_10, :s->s_ytd, :s->s_order_cnt, :s->s_remote_cnt, :s->s_data);

			if (!check_status(isc_status))
				break;

			CheckForCommit(&tr, ++rows_loaded, "stock", &time_start);
		}
	}

	printf("Finished loading stock table.\n");

	return ISC_STATUS_SUCCEEDED(isc_status);
}

//=======================================================================
//
// Function : CustomerBufInit
//
//=======================================================================
void CustomerBufInit()
{
	int i;
	for (i = 0; i < customers_per_district; i++)
	{
		customer_buf[i].c_id = 0;
		customer_buf[i].c_d_id = 0;
		customer_buf[i].c_w_id = 0;
		strcpy(customer_buf[i].c_first,"");
		strcpy(customer_buf[i].c_middle,"");
		strcpy(customer_buf[i].c_last,"");
		strcpy(customer_buf[i].c_street_1,"");
		strcpy(customer_buf[i].c_street_2,"");
		strcpy(customer_buf[i].c_city,"");
		strcpy(customer_buf[i].c_state,"");
		strcpy(customer_buf[i].c_zip,"");
		strcpy(customer_buf[i].c_phone,"");
		strcpy(customer_buf[i].c_credit,"");
		customer_buf[i].c_credit_lim = 0;
		customer_buf[i].c_discount = 0;
		customer_buf[i].c_balance = 0;
		customer_buf[i].c_ytd_payment = 0;
		customer_buf[i].c_payment_cnt = 0;
		customer_buf[i].c_delivery_cnt = 0;
		strcpy(customer_buf[i].c_data,"");
		customer_buf[i].h_amount = 0;
		strcpy(customer_buf[i].h_data,"");
	}
}

//=======================================================================
//
// Function : CustomerBufLoad
//
// Fills shared buffer for HISTORY and CUSTOMER
//=======================================================================
void CustomerBufLoad(int d_id, int w_id)
{
	long i;
	CUSTOMER_SORT_STRUCT c[CUSTOMERS_PER_DISTRICT];

	for (i = 0; i < customers_per_district; i++)
	{
		if (i < 1000)
			LastName(i, c[i].c_last);
		else
			LastName(NURand(255, 0, 999, LOADER_NURAND_C), c[i].c_last);
		MakeAlphaString(8,16, MAX_FIRST_NAME_LEN, c[i].c_first);
		c[i].c_id = i + 1;
	}

	printf("...Loading customer buffer for: d_id = %d, w_id = %d\n",
		d_id, w_id);

	for (i = 0; i < customers_per_district; i++)
	{
		customer_buf[i].c_d_id = d_id;
		customer_buf[i].c_w_id = w_id;
		customer_buf[i].h_amount = 10 * MONEY_SCALE;
		customer_buf[i].c_ytd_payment = 10 * MONEY_SCALE;
		customer_buf[i].c_payment_cnt = 1;
		customer_buf[i].c_delivery_cnt = 0;
		// Generate CUSTOMER and HISTORY data
		customer_buf[i].c_id = c[i].c_id;
		strcpy(customer_buf[i].c_first, c[i].c_first);
		strcpy(customer_buf[i].c_last, c[i].c_last);
		customer_buf[i].c_middle[0] = 'O';
		customer_buf[i].c_middle[1] = 'E';
		MakeAddress(customer_buf[i].c_street_1, customer_buf[i].c_street_2,
			customer_buf[i].c_city, customer_buf[i].c_state, customer_buf[i].c_zip);
		MakeNumberString(16, 16, MAX_PHONE_LEN, customer_buf[i].c_phone);
		if (RandomNumber(1L, 100L) > 10)
			customer_buf[i].c_credit[0] = 'G';
		else
			customer_buf[i].c_credit[0] = 'B';
		customer_buf[i].c_credit[1] = 'C';
		customer_buf[i].c_credit_lim = 50000 * MONEY_SCALE;
		customer_buf[i].c_discount = RandomNumber(0, 5000); // SCALE 2
		customer_buf[i].c_balance = -10 * MONEY_SCALE;
		MakeAlphaString(300, 500, MAX_CUSTOMER_DATA_LEN, customer_buf[i].c_data);
		// Generate HISTORY data
		MakeAlphaString(12, 24, MAX_HISTORY_DATA_LEN, customer_buf[i].h_data);
	}
}

//=======================================================================
//
// Function : LoadCustomerTable
//
//=======================================================================

typedef struct
{
    ISC_ULONG time_start;
    ISC_ULONG rows_loaded;
} PROGRESS_STRUCT;

bool_t LoadCustomerTable(
	isc_db_handle db,
	isc_tr_handle tr,
	PROGRESS_STRUCT* progress)
{
	ISC_STATUS isc_status[20];
	CUSTOMER_STRUCT *c;
	int i;

	for (i = 0; i < customers_per_district; i++)
	{
		c = &customer_buf[i];

		current_timestamp(&c->c_since);

		EXEC SQL 
			INSERT TRANSACTION tr INTO db.customer (
				c_id, c_d_id, c_w_id, c_first, c_middle, c_last, c_street_1, c_street_2, 
				c_city, c_state, c_zip, c_phone, c_since, c_credit, c_credit_lim, c_discount, 
				c_balance, c_ytd_payment, c_payment_cnt, c_delivery_cnt, c_data)
			VALUES (:c->c_id, :c->c_d_id, :c->c_w_id, :c->c_first, :c->c_middle, :c->c_last, :c->c_street_1, :c->c_street_2, 
				:c->c_city, :c->c_state, :c->c_zip, :c->c_phone, :c->c_since, :c->c_credit, :c->c_credit_lim, :c->c_discount, 
				:c->c_balance, :c->c_ytd_payment, :c->c_payment_cnt, :c->c_delivery_cnt, :c->c_data);
		
		if (!check_status(isc_status))
			break;

		CheckForCommit(&tr, ++progress->rows_loaded, "customer", &progress->time_start);
	}

	return ISC_STATUS_SUCCEEDED(isc_status);
}

//=======================================================================
//
// Function : LoadHistoryTable
//
//=======================================================================
bool_t LoadHistoryTable(
	isc_db_handle db,
	isc_tr_handle tr,
	PROGRESS_STRUCT* progress)
{
	ISC_STATUS isc_status[20];
	CUSTOMER_STRUCT *c;
	int i;

	for (i = 0; i < customers_per_district; i++)
	{
		datetime_t h_date;

		c = &customer_buf[i];
		current_timestamp(&h_date);

		EXEC SQL 
			INSERT TRANSACTION tr INTO db.history (h_c_id, h_c_d_id, h_c_w_id, h_d_id, h_w_id, h_date, h_amount, h_data)
			VALUES (:c->c_id, :c->c_d_id, :c->c_w_id, :c->c_d_id, :c->c_w_id, :h_date, :c->h_amount, :c->h_data)

		if (!check_status(isc_status))
			break;

		CheckForCommit(&tr, ++progress->rows_loaded, "history", &progress->time_start);
	}

	return ISC_STATUS_SUCCEEDED(isc_status);
}

//=======================================================================
//
// Function : LoadCustomer
//
//=======================================================================
bool_t Customer(
	isc_db_handle db,
	isc_tr_handle tr)
{
	ISC_STATUS isc_status[20];
	bool_t ok = TRUE;
	PROGRESS_STRUCT customer_progress_struct;
	PROGRESS_STRUCT history_progress_struct;
	short w_id;
	short d_id;
	char buffer[32];

	seed(5);
	printf("Loading customer and history tables...\n");

	CustomerBufInit();

	customer_progress_struct.rows_loaded = 0;
	customer_progress_struct.time_start = TimeNow();

	history_progress_struct.rows_loaded = 0;
	history_progress_struct.time_start = TimeNow();

	for (w_id = aptr->starting_warehouse; w_id <= aptr->num_warehouses; w_id++)
	{
		for (d_id = 1; d_id <= DISTRICT_PER_WAREHOUSE; d_id++)
		{
			CustomerBufLoad(d_id, w_id);

			printf("...Loading customer table for: d_id = %d, w_id = %d\n", d_id, w_id);
			ok = LoadCustomerTable(db, tr, &customer_progress_struct);
			if (!ok)
				break;

			printf("...Loading history table for: d_id = %d, w_id = %d\n", d_id, w_id);
			ok = LoadHistoryTable(db, tr, &history_progress_struct);
			if (!ok)
				break;
		}
	}

	printf("Finished loading customer and history tables.\n");

	// Output the NURAND used for the loader into C_FIRST for C_ID = 1, C_W_ID = 1, and C_D_ID = 1
	sprintf(buffer, "C_LOAD = %d", LOADER_NURAND_C);

	EXEC SQL 
		UPDATE TRANSACTION tr customer SET c_first = :buffer
		WHERE c_id = 1 AND c_w_id = 1 AND c_d_id = 1;

	check_status(isc_status);

	return ok;
}

//=======================================================================
//
// Function : OrdersBufInit
//
// Clears shared buffer for ORDERS, NEWORDER, and ORDERLINE
//
//=======================================================================
void OrdersBufInit()
{
	int i, j;
	for (i = 0; i < orders_per_district; i++)
	{
		orders_buf[i].o_id = 0;
		orders_buf[i].o_d_id = 0;
		orders_buf[i].o_w_id = 0;
		orders_buf[i].o_c_id = 0;
		orders_buf[i].o_carrier_id = 0;
		orders_buf[i].o_ol_cnt = 0;
		orders_buf[i].o_all_local = 0;
		for (j = 0; j <= 14; j++)
		{
			orders_buf[i].o_ol[j].ol_number = 0;
			orders_buf[i].o_ol[j].ol_i_id = 0;
			orders_buf[i].o_ol[j].ol_supply_w_id = 0;
			orders_buf[i].o_ol[j].ol_quantity = 0;
			orders_buf[i].o_ol[j].ol_amount = 0;
			strcpy(orders_buf[i].o_ol[j].ol_dist_info,"");
		}
	}
}

//=======================================================================
//
// Function : OrdersBufLoad
//
// Fills shared buffer for ORDERS, NEWORDER, and ORDERLINE
//
//=======================================================================
void OrdersBufLoad(int d_id, int w_id)
{
	int cust[ORDERS_PER_DISTRICT + 1];
	long o_id;
	short ol;
	printf("...Loading Order Buffer for: d_id = %d, w_id = %d\n",
		d_id, w_id);
	GetPermutation(cust, orders_per_district);
	for (o_id = 0; o_id < orders_per_district; o_id++)
	{
		// Generate ORDER and NEW- ORDER data
		orders_buf[o_id].o_d_id = d_id;
		orders_buf[o_id].o_w_id = w_id;
		orders_buf[o_id].o_id = o_id + 1;
		orders_buf[o_id].o_c_id = cust[o_id + 1];
		orders_buf[o_id].o_ol_cnt = (short) RandomNumber(5L, 15L);
		if (o_id < first_new_order)
		{
			orders_buf[o_id].o_carrier_id = (short) RandomNumber(1L, 10L);
			orders_buf[o_id].o_all_local = 1;
		}
		else
		{
			orders_buf[o_id].o_carrier_id = 0;
			orders_buf[o_id].o_all_local = 1;
		}
		for (ol = 0; ol < orders_buf[o_id].o_ol_cnt; ol++)
		{
			orders_buf[o_id].o_ol[ol].ol_number = ol + 1;
			orders_buf[o_id].o_ol[ol].ol_i_id = RandomNumber(1L, max_items);
			orders_buf[o_id].o_ol[ol].ol_supply_w_id = w_id;
			orders_buf[o_id].o_ol[ol].ol_quantity = 5;
			MakeAlphaString(24, 24, MAX_DIST_LEN, 
				orders_buf[o_id].o_ol[ol].ol_dist_info);
			// Generate ORDER-LINE data
			if (o_id < first_new_order)
			{
				orders_buf[o_id].o_ol[ol].ol_amount = 0;
				// Added to insure ol_delivery_d set properly during load
				current_timestamp(&orders_buf[o_id].o_ol[ol].ol_delivery_d);
			}
			else
			{
				orders_buf[o_id].o_ol[ol].ol_amount = RandomNumber(1,999999); // SCALE 2
				// Added to insure ol_delivery_d set properly during load
				// odbc datetime format
				zero_timestamp(&orders_buf[o_id].o_ol[ol].ol_delivery_d);
			}
		}
	}
}

//=======================================================================
//
// Function : LoadOrdersTable
//
//=======================================================================
bool_t LoadOrdersTable(
	isc_db_handle db,
	isc_tr_handle tr,
	PROGRESS_STRUCT *progress)
{
	ISC_STATUS isc_status[20];
	int i;

	for (i = 0; i < orders_per_district; i++)
	{
		ORDERS_STRUCT* o = &orders_buf[i];
		datetime_t o_entry_d;

		current_timestamp(&o_entry_d);

		EXEC SQL
			INSERT TRANSACTION tr INTO db.orders (o_id, o_d_id, o_w_id, o_c_id, 
				o_entry_d, o_carrier_id, o_ol_cnt, o_all_local)
			VALUES (:o->o_id, :o->o_d_id, :o->o_w_id, :o->o_c_id, 
				:o_entry_d, :o->o_carrier_id, :o->o_ol_cnt, :o->o_all_local);

		if (!check_status(isc_status))
			break;

		CheckForCommit(&tr, ++progress->rows_loaded, "orders", &progress->time_start);
	}

	return ISC_STATUS_SUCCEEDED(isc_status);
}

//=======================================================================
//
// Function : LoadNewOrderTable
//
//=======================================================================
bool_t LoadNewOrderTable(
	isc_db_handle db,
	isc_tr_handle tr,
	PROGRESS_STRUCT *progress)
{
	ISC_STATUS isc_status[20];
	int i;

	for (i = first_new_order; i < last_new_order; i++)
	{
		ORDERS_STRUCT* o = &orders_buf[i];
		
		EXEC SQL
			INSERT TRANSACTION tr INTO db.new_order (no_o_id, no_d_id, no_w_id)
			VALUES (:o->o_id, :o->o_d_id, :o->o_w_id);

		if (!check_status(isc_status))
			break;

		CheckForCommit(&tr, ++progress->rows_loaded, "new_order", &progress->time_start);
	}

	return ISC_STATUS_SUCCEEDED(isc_status);
}

//=======================================================================
//
// Function : LoadOrderLineTable
//
//=======================================================================
bool_t LoadOrderLineTable(
	isc_db_handle db,
	isc_tr_handle tr,
	PROGRESS_STRUCT *progress)
{
	ISC_STATUS isc_status[20];
	int i, j;

	for (i = 0; i < orders_per_district; i++)
	{
		ORDERS_STRUCT* o = &orders_buf[i];

		for (j = 0; j < o->o_ol_cnt; j++)
		{
			ORDER_LINE_STRUCT *ol = &o->o_ol[j]; 

			EXEC SQL
				INSERT TRANSACTION tr INTO db.order_line (ol_o_id, ol_d_id, ol_w_id, ol_number, ol_i_id, 
					ol_supply_w_id, ol_delivery_d, ol_quantity, ol_amount, ol_dist_info)
			VALUES (:o->o_id, :o->o_d_id, :o->o_w_id, :ol->ol_number, :ol->ol_i_id, 
					:ol->ol_supply_w_id, :ol->ol_delivery_d, :ol->ol_quantity, :ol->ol_amount, :ol->ol_dist_info);

			if (!check_status(isc_status))
				break;

			CheckForCommit(&tr, ++progress->rows_loaded, "order_line", &progress->time_start);
		}
	}

	return ISC_STATUS_SUCCEEDED(isc_status);
}

//========================================================================
//
// Function : LoadOrders
//
//========================================================================
bool_t Orders(
	isc_db_handle db,
	isc_tr_handle tr)
{
	bool_t ok = TRUE;
	PROGRESS_STRUCT orders_progress_struct;
	PROGRESS_STRUCT new_order_progress_struct;
	PROGRESS_STRUCT order_line_progress_struct;
	short w_id;
	short d_id;

	// seed with unique number
	seed(6);

	orders_progress_struct.rows_loaded = 0;
	orders_progress_struct.time_start = TimeNow();

	new_order_progress_struct.rows_loaded = 0;
	new_order_progress_struct.time_start = TimeNow();
	
	order_line_progress_struct.rows_loaded = 0;
	order_line_progress_struct.time_start = TimeNow();

	OrdersBufInit();

	for (w_id = aptr->starting_warehouse; w_id <= aptr->num_warehouses; w_id++)
	{
		for (d_id = 1; d_id <= DISTRICT_PER_WAREHOUSE; d_id++)
		{
			OrdersBufLoad(d_id, w_id);

			printf("...Loading Order Table for: d_id = %d, w_id = %d\n", d_id, w_id);
			ok = LoadOrdersTable(db, tr, &orders_progress_struct);
			if (!ok)
				break;

			printf("...Loading New-Order Table for: d_id = %d, w_id = %d\n", d_id, w_id);
			ok = LoadNewOrderTable(db, tr, &new_order_progress_struct);
			if (!ok)
				break;

			printf("...Loading Order-Line Table for: d_id = %d, w_id = %d\n", d_id, w_id);
			ok = LoadOrderLineTable(db, tr, &new_order_progress_struct);
			if (!ok)
				break;
		}
	}

	printf("Finished loading orders.\n");

	return ok;
}

//=======================================================================
//
// Function : GetPermutation
//
//=======================================================================
void GetPermutation(int perm[], int n)
{
	int i, r, t;
	for (i= 1; i<= n; i++)
		perm[i] = i;
	for (i= 1; i<= n; i++)
	{
		r = RandomNumber(i, n);
		t = perm[i];
		perm[i] = perm[r];
		perm[r] = t;
	}
}

//=======================================================================
//
// Function : CheckForCommit
//
//=======================================================================
void CheckForCommit(
	isc_tr_handle* tr_handle,
	int rows_loaded,
	const char *table_name,
    ISC_ULONG *time_start)
{
	long time_end, time_diff;

	if (!(rows_loaded % aptr->batch))
	{
		ISC_STATUS isc_status[20];

		isc_commit_retaining(
			isc_status,
			tr_handle);

		check_status(isc_status);

		time_end = TimeNow();
		time_diff = time_end - *time_start;
		printf("->Loaded %ld rows into %s in %.3f sec - Total = %d (%.2f rps)\n",
			aptr->batch, table_name, (double) time_diff / MILLI, rows_loaded,
			(double) aptr->batch * MILLI / (time_diff ? time_diff : 1L));
		*time_start = time_end;
	}
}

const LoadData load_item = {Item, "item"};
const LoadData load_warehouse = {Warehouse, "warehouse"};
const LoadData load_district = {District, "district"};
const LoadData load_stock = {Stock, "stock"};
const LoadData load_customer = {Customer, "customer"};
const LoadData load_orders = {Orders, "orders"};

//=======================================================================
//
// Function name: main
//
//=======================================================================
int main(int argc, char ** argv)
{
	DWORD dwThreadID[MAX_MAIN_THREADS];
	HANDLE hThread[MAX_MAIN_THREADS];
	int i;
	for (i = 0; i < MAX_MAIN_THREADS; i++)
		hThread[i] = 0;

	// process command line arguments
	aptr = &args;
	GetArgsLoader(argc, argv, aptr);

	// set database scale values
	if (aptr->scale_down == 1)
	{
		printf("*** Scaled Down Database ***\n");
		max_items = MAXITEMS_SCALE_DOWN;
		customers_per_district = CUSTOMERS_SCALE_DOWN;
		orders_per_district = ORDERS_SCALE_DOWN;
		first_new_order = 0;
		last_new_order = 30;
	}
	else
	{
		max_items = MAXITEMS;
		customers_per_district = CUSTOMERS_PER_DISTRICT;
		orders_per_district = ORDERS_PER_DISTRICT;
		first_new_order = 2100;
		last_new_order = 3000;
	}

	// start loading data
	printf("TPC-C load started for %ld warehouses.\n", aptr->num_warehouses);

	main_time_start = TimeNow();

#ifdef _WINDOWS
	InitializeCriticalSection(&isc_attach_cs);
#endif

	// start parallel load threads
	if (aptr->tables_all || aptr->table_item)
	{
		printf("\nStarting loader threads for: item\n");

		hThread[0] = CreateThread(NULL, 0,
			LoadThread, (void*) &load_item, 0, &dwThreadID[0]);

		if (hThread[0] == 0)
			printf("Error, failed in creating creating thread = 0.\n");
	}

	if (aptr->tables_all || aptr->table_warehouse)
	{
		printf("Starting loader threads for: warehouse\n");

		hThread[1] = CreateThread(NULL, 0, LoadThread, (void*) &load_warehouse,
			0, &dwThreadID[1]);

		if (hThread[1] == 0)
			printf("Error, failed in creating creating thread = 1.\n");
	}

	if (aptr->tables_all || aptr->table_customer)
	{
		printf("Starting loader threads for: customer\n");

		hThread[2] = CreateThread(NULL, 0, LoadThread, (void*) &load_customer,
			0, &dwThreadID[2]);

		if (hThread[2] == 0)
			printf("Error, failed in creating creating thread = 2.\n");
	}

	if (aptr->tables_all || aptr->table_orders)
	{
		printf("Starting loader threads for: orders\n");

		hThread[3] = CreateThread(NULL, 0, LoadThread, (void*) &load_orders,
			0, &dwThreadID[3]);

		if (hThread[4] == 0)
			printf("Error, failed in creating creating thread = 2.\n");
	}

	// Wait for threads to finish...
	for (i= 0; i < MAX_MAIN_THREADS; i++)
	{
		if (hThread[i] != 0)
		{
#ifdef _WINDOWS
            WaitForSingleObject(hThread[i], INFINITE);
            CloseHandle(hThread[i]);
#else
            pthread_join(hThread[i], 0);
#endif
            hThread[i] = 0;
        }
	}

#ifdef _WINDOWS
	DeleteCriticalSection(&isc_attach_cs);
#endif

	main_time_end = TimeNow();
	
	printf("\nTPC-C load completed successfully in %.3f seconds.\n",
		(double)(main_time_end - main_time_start) / MILLI);

	return 0;
}


        