// Build number of TPC Benchmark Kit
//#define TPCKIT_VER "4.21"

#include "../tpcc.h"

// General constants
#define UNDEF -1
#define BATCH 10000
//#define MINPRINTASCII 32
//#define MAXPRINTASCII 126

// Default environment constants
#define SERVER ""
#define DATABASE "tpcc"
#define USER "sysdba"
#define PASSWORD "masterke"

// Default loader arguments
#define LOADER_NURAND_C 123
#define DEF_STARTING_WAREHOUSE 1
#define SCALE_DOWN 0 // build a normal scale database

typedef struct
{
	const char *server;
	const char *database;
	const char *user;
	const char *password;
	BOOL tables_all; // set if loading all tables
	BOOL table_item; // set if loading ITEM table specifically
	BOOL table_warehouse; // set if loading WAREHOUSE, DISTRICT, and STOCK
	BOOL table_customer; // set if loading CUSTOMER and HISTORY
	BOOL table_orders; // set if loading NEW- ORDER, ORDERS, ORDER- LINE
	long num_warehouses;
	long batch;
	long verbose;
	long pack_size;
	const char *loader_res_file;
	const char *synch_servername;
	long case_sensitivity;
	long starting_warehouse;
	long build_index;
	long index_order;
	long scale_down;
	const char *index_script_path;
} TPCCLDR_ARGS;

// String length constants
#define SERVER_NAME_LEN 20
#define DATABASE_NAME_LEN 20
#define USER_NAME_LEN 20
#define PASSWORD_LEN 20
#define TABLE_NAME_LEN 20


extern "C"
{

// Functions in random.c
void seed(long val);
long irand();
double drand();
void WUCreate();
short WURand();
long RandomNumber( long lower, long upper);
// Functions in getargs.c;
void GetArgsLoader(int argc, char ** argv, TPCCLDR_ARGS *pargs);
void GetArgsLoaderUsage();
// Functions in strings.c
//void MakeAddress();
//void LastName();
//int MakeAlphaString();
//int MakeOriginalAlphaString();
//int MakeNumberString();
//int MakeZipNumberString();
//void InitString();
void InitAddress(char *street_1, char *street_2,
    char *city, char *state, char *zip);
void PaddString();

};
