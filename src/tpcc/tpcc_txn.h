#ifndef __TPCC_TXN_H
#define __TPCC_TXN_H

#ifdef __cplusplus
extern "C" {
#endif

// transaction data

typedef struct
{
	d_id_t d_id;
	c_id_t c_id;
	short o_ol_cnt;
} new_order_input;

typedef struct
{
	i_id_t ol_i_id;
	w_id_t ol_supply_w_id;
	int ol_quantity;
} new_order_input_repeat;

typedef struct
{
	o_id_t o_id; 
	char c_last[MAX_LAST_NAME_LEN + 1];
	char c_credit[CREDIT_LEN + 1];
	percent_t c_discount; 
	percent_t w_tax; 
	percent_t d_tax; 
	datetime_t o_entry_d; 
	amount_t total_amount; 
} new_order_output;

typedef struct
{
	char i_name[MAX_ITEM_NAME_LEN + 1]; 
	short s_quantity;
	char brand_generic; 
	int i_price; 
	amount_t ol_amount;
} new_order_output_repeat;

typedef struct
{
	d_id_t d_id;
	w_id_t c_w_id;
	d_id_t c_d_id;
	c_id_t c_id; // 0 if by name
	char c_last[MAX_LAST_NAME_LEN + 1];
	amount_t h_amount;
} payment_input;

typedef struct
{
	datetime_t h_date;

	char w_name[MAX_WAREHOUSE_NAME_LEN + 1];
	char w_street_1[MAX_STREET_LEN + 1];
	char w_street_2[MAX_STREET_LEN + 1];
	char w_city[MAX_CITY_LEN + 1];
	char w_state[MAX_STATE_LEN + 1];
	char w_zip[MAX_ZIP_LEN + 1];

	char d_name[MAX_DISTRICT_NAME_LEN + 1];
	char d_street_1[MAX_STREET_LEN + 1];
	char d_street_2[MAX_STREET_LEN + 1];
	char d_city[MAX_CITY_LEN + 1];
	char d_state[MAX_STATE_LEN + 1];
	char d_zip[MAX_ZIP_LEN + 1];

	c_id_t c_id;
	char c_first[MAX_FIRST_NAME_LEN + 1];
	char c_middle[MAX_MIDDLE_NAME_LEN + 1];
	char c_last[MAX_LAST_NAME_LEN + 1];
	char c_street_1[MAX_STREET_LEN + 1];
	char c_street_2[MAX_STREET_LEN + 1];
	char c_city[MAX_CITY_LEN + 1];
	char c_state[MAX_STATE_LEN + 1];
	char c_zip[MAX_ZIP_LEN + 1];
	char c_phone[MAX_PHONE_LEN + 1];
	datetime_t c_since;
	char c_credit[CREDIT_LEN + 1];
	amount_t c_credit_lim;
	percent_t c_discount;
	amount_t c_balance;
	char c_data[MAX_CUSTOMER_DATA_LEN + 1];

} payment_output;

typedef struct
{
	d_id_t d_id;
	c_id_t c_id; // 0 i by name
	char c_last[MAX_LAST_NAME_LEN + 1];
} ostat_input;

typedef struct
{
	c_id_t c_id;
	char c_first[MAX_FIRST_NAME_LEN + 1];
	char c_middle[MAX_MIDDLE_NAME_LEN + 1];
	char c_last[MAX_LAST_NAME_LEN + 1];
	amount_t c_balance;
	o_id_t o_id;
	datetime_t o_entry_d;
	carrier_id_t o_carrier_id;
	short cnt;
} ostat_output;

typedef struct
{
	w_id_t ol_supply_w_id;
	i_id_t ol_i_id;
	quantity_t ol_quantity;
	amount_t ol_amount;
	datetime_t ol_delivery_d;
} ostat_output_repeat;

typedef struct
{
	carrier_id_t o_carrier_id;
} delivery_input;

typedef struct
{
	d_id_t d_id;                /* must be fixed for a terminal */
	int threshold;
} slev_input;

typedef struct
{
	int stock_count;
} slev_output;

// transaction prototypes

int neword(
	isc_db_handle db,
	Diag* diag,
	w_id_t w_id,
	const new_order_input* in,
	const new_order_input_repeat* in_rpt,
	new_order_output* out,
	new_order_output_repeat* out_rpt);

int payment(
	isc_db_handle db,
	Diag* diag,
	w_id_t w_id,
	const payment_input* in,
	payment_output* out);

int ostat(
	isc_db_handle db,
	Diag* diag,
	w_id_t w_id,
	const ostat_input* in,
	ostat_output* out,
	ostat_output_repeat* out_rpt); /* MAX_ORDER_ITEMS */

int delivery(
	isc_db_handle db,
	Diag* diag,
	w_id_t w_id,
	const delivery_input* in);

int slev(
	isc_db_handle db,
	Diag* diag,
	w_id_t w_id,
	const slev_input* in,
	slev_output* out);

#ifdef __cplusplus
}
#endif

#endif
