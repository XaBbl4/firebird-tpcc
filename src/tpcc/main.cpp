#include "stdafx.h"

#include <assert.h>
#include <math.h>
#include <algorithm>

#include <ibase.h>

#ifndef _WINDOWS
#include <semaphore.h>
#include <errno.h>
#include <fcntl.h>
#else
#include <process.h>
#endif

#include "../tpcc.h"
#include "tpcc_txn.h"


static int v_printf(const char*, ...);

#define lengthof(x) (sizeof(x)/sizeof(x[0]))

#define DIALECT 3

#define BIG_TIME (1000 * TIME_SCALE)
#define TIME_SCALE 1000 // milliseconds

#define for if (0) ; else for 

const char* DEF_DATABASE = "TPCC.GDB";
const char* DEF_USER = "SYSDBA";
const char* DEF_PASSWORD = "masterkey";

#define DEF_WAREHOUSES 1
#define DEF_STARING_WAREHOUSE 1
#define DEF_TERMINALS 1
#define DEF_RUN_TIME 150
#define DEF_RUMP_UP_TIME 30
#define DEF_REPORT_INTERVAL 30
#define DEF_RESPONSE_TIME_INTERVALS 200
#define DEF_RESPONSE_TIME_LIMIT 20
#define DEF_THINK_TIME_INTERVALS 200
#define DEF_THINK_TIME_LIMIT 20

enum txn_t
{
    NEW_ORDER = 0,
    PAYMENT,
	ORDER_STATUS,
	DELIVERY,
	STOCK_LEVEL,
	_TXN_COUNT
};

struct TxnData
{
	const char *Name;
	ISC_ULONG KeyingTime;  /* ms */
	ISC_ULONG MeanThinkTime;    /* ms */
};

const struct TxnData txn_data[_TXN_COUNT] =
{
	{"new order",       12 * TIME_SCALE, 18 * TIME_SCALE},
	{"payment",        12 * TIME_SCALE, 3 * TIME_SCALE},
	{"order status",    10 * TIME_SCALE, 2 * TIME_SCALE},
	{"delivery",         5 * TIME_SCALE, 2 * TIME_SCALE},
	{"stock level",      5 * TIME_SCALE, 2 * TIME_SCALE}
};

static
ISC_ULONG get_think_time(
	enum txn_t txn_type)
{
	double mean = txn_data[txn_type].MeanThinkTime;
	double x = drand();
	if (x < 4.54e-5)
		x = 4.54e-5;
	return (ISC_ULONG)(-log(x) * mean);
}

struct Config
{
	char Database [200];
	char User [200];
	char Password [200];
	ISC_ULONG Terminals;
	ISC_ULONG Warehouses;
	ISC_ULONG StartingWarehouse;
	ISC_ULONG RunTime;
	ISC_ULONG RumpUpTime;
	ISC_ULONG ReportInterval;
	ISC_ULONG ResponseTimeIntervals;
	ISC_ULONG ResponseTimeLimit;
	ISC_ULONG ThinkTimeIntervals;
	ISC_ULONG ThinkTimeLimit;
	bool ScaleDown;

	void Init (
		int argc, 
		char ** argv);
};


class Deck
{
public:

	Deck()
	{
		m_Index = CARDS;
	}

	txn_t Draw ()
	{
		if (m_Index >= CARDS)
			Deal();

		return (m_Cards[m_Index++].Txn);
	}

	struct Card
	{
		ISC_ULONG Random;
		txn_t Txn;
	};

protected:

	void Deal();

	enum 
	{
		CARDS = 100,
		STOCK_LEVEL_CARDS = 4,
		DELIVERY_CARDS = 4,
		ORDER_STATUS_CARDS = 4,
		PAYMENT_CARDS = 43,
		NEW_ORDER_CARDS = 45
	};

	Card m_Cards[CARDS];
	ISC_ULONG m_Index;
};

//namespace std
//{

inline
bool operator < (
	const Deck::Card& x, 
	const Deck::Card& y)
{
	return x.Random < y.Random;
}

//b}

void Deck::Deal ()
{
	ISC_ULONG i;
	Card* p = m_Cards;

	for (i = 0; i < STOCK_LEVEL_CARDS; ++i)
		(p++)->Txn = STOCK_LEVEL;
	for (i = 0; i < DELIVERY_CARDS; ++i)
		(p++)->Txn = DELIVERY;
	for (i = 0; i < ORDER_STATUS_CARDS; ++i)
		(p++)->Txn = ORDER_STATUS;
	for (i = 0; i < PAYMENT_CARDS; ++i)
		(p++)->Txn = PAYMENT;
	for (i = 0; i < NEW_ORDER_CARDS; ++i)
		(p++)->Txn = NEW_ORDER;

	p = m_Cards;
	for (i = 0; i < CARDS; ++i)
		(p++)->Random = RandomNumber(1, 10000);

	std::sort(m_Cards, m_Cards + CARDS);

	m_Index = 0;
}

static Mutex printfLock;

template <class T>
class LockGuard
{
	T* m_p;
public:
	LockGuard(T* p) : m_p(p) { p->Lock(); }
	~LockGuard() { m_p->Unlock(); }
};

#ifdef _WINDOWS
class AHandle
{
	HANDLE m_h;
public:
	AHandle() : m_h(NULL) {}
	~AHandle() { if (m_h) ::CloseHandle(m_h); }
	operator HANDLE () const { return m_h; }
	void operator = (HANDLE h) { if (m_h) ::CloseHandle(m_h); m_h = h; }
};
typedef AHandle SemHandle;
#else
typedef pthread_t AHandle;
typedef sem_t SemHandle;
const int WAIT_OBJECT_0 = 0;
#endif

#ifndef _WINDOWS
static timespec getCurrentTime()
{
	timespec rc;

#ifdef HAVE_GETTIMEOFDAY
	struct timeval tp;
	GETTIMEOFDAY(&tp);
	rc.tv_sec = tp.tv_sec;
	rc.tv_nsec = tp.tv_usec * 1000;
#else
	struct timeb time_buffer;
	ftime(&time_buffer);
	rc.tv_sec = time_buffer.time;
	rc.tv_nsec = time_buffer.millitm * 1000000;
#endif

	return rc;
}
#endif

class Stats
{
public:

	Stats()
	{
	}

	~Stats()
	{
//		if (m_Hist != NULL)
//			delete [] m_Hist;
	}

	void Init(
		ISC_ULONG Intervals,
		ISC_ULONG Limit)
	{
		// initialization
		m_ReportIntervals = Intervals;
		m_Hist = new ISC_ULONG [Intervals];
		m_Limit = Limit;
		
		Reset();
	}

	void Reset ()
	{
		m_Total = 0;
		m_Min = BIG_TIME;
		m_Max = 0;
		memset (m_Hist, 0, m_ReportIntervals * sizeof(ISC_ULONG));
	}

	void Put (
		ISC_ULONG Time)
	{
		if (Time < m_Min)
			m_Min = Time;

		if (Time > m_Max)
			m_Max = Time;

		m_Total += Time;

		ISC_ULONG i = ISC_ULONG(((double) Time / m_Limit) * m_ReportIntervals);

		if (i < m_ReportIntervals)
			m_Hist[i]++;
	}

	ISC_ULONG getIntervalCount() const
	{
		return m_ReportIntervals;
	}

	ISC_ULONG getValue(
		ISC_ULONG Index) const
	{
		assert(Index < m_ReportIntervals);
		return m_Hist[Index];
	}

	double getPercentile (
		double x,
		ISC_ULONG Count) const
	{
		const double x90 = (double) Count * x;
		double sum = 0;

		ISC_ULONG i;
		for (i = 0; i < m_ReportIntervals; ++i)
		{
			sum += m_Hist[i];

			if ((double) sum > x90)
				break;
		}

		return (double) (i + 0.5) / m_ReportIntervals;
	}


	ISC_ULONG getMax() const { return m_Max; }
	ISC_ULONG getMin() const { return m_Min; }
	ISC_ULONG getTotal() const { return m_Total; }
	
protected:

	ISC_ULONG m_ReportIntervals;
	ISC_ULONG m_Limit;
	ISC_ULONG m_Total;
	ISC_ULONG m_Min;
	ISC_ULONG m_Max;
	ISC_ULONG* m_Hist;
};

class TxnStats
{
public:

	TxnStats ()
	{
		m_Count = 0;
	}

	void Init (
		ISC_ULONG ResponseTimeIntervals,
		ISC_ULONG ResponseTimeLimit,
		ISC_ULONG ThinkTimeIntervals,
		ISC_ULONG ThinkTimeLimit)
	{
		m_Count = 0;
		m_RolledbackCount = 0;
		m_Keying.Init(0, 0);
		m_Response.Init(ResponseTimeIntervals, ThinkTimeLimit);
		m_Think.Init(ThinkTimeIntervals, ThinkTimeLimit);
	}

	void Reset ()
	{
		m_Count = 0;
		m_RolledbackCount = 0;
		m_Keying.Reset();
		m_Response.Reset();
		m_Think.Reset();
	}

	void PutTransaction (
		ISC_ULONG KeyingTime,
		ISC_ULONG ResponseTime,
		ISC_ULONG ThinkTime,
		bool rollback)
	{
		m_Count ++;

		if (rollback)
			m_RolledbackCount ++;

		m_Keying.Put(KeyingTime);
		m_Response.Put(ResponseTime);
		m_Think.Put(ThinkTime);
	}

	ISC_ULONG getCount() const { return m_Count; }
	ISC_ULONG getRolledbackCount() const { return m_RolledbackCount; }
	const Stats& getKeying() const { return m_Keying; }
	const Stats& getResponse() const { return m_Response; }
	const Stats& getThink() const { return m_Think; }
	
protected:

	ISC_ULONG m_Count;
	ISC_ULONG m_RolledbackCount;
	Stats m_Keying;
	Stats m_Response;
	Stats m_Think;
};

class Monitor;
class Terminal;


class Stmt
{
public:
	
	Stmt()
	{
		m_stmt = 0;
		m_sqlda_in = NULL;
		m_sqlda_out = NULL;
	}
	
	void Init(
		Terminal* terminal)
	{
		m_Terminal = terminal;
	}
	
	bool Prepare(
		Diag& diag,
		isc_tr_handle* txn,
		const char* sql,
		ISC_ULONG in,
		ISC_ULONG out,
		short dialect);

	void BindInput(
		ISC_ULONG Index,
		short Type,
		unsigned short Length,
		const void* Data)
	{
		m_sqlda_in->sqlvar[Index].sqltype = Type & ~1;
		m_sqlda_in->sqlvar[Index].sqllen = Length;
		m_sqlda_in->sqlvar[Index].sqldata = (char*) Data;
		m_sqlda_in->sqlvar[Index].sqlind = NULL;
	}

	void BindOutput(
		ISC_ULONG Index,
		short Type,
		unsigned short Length,
		void* Data)
	{
		m_sqlda_out->sqlvar[Index].sqltype = Type & ~1;
		m_sqlda_out->sqlvar[Index].sqllen = Length;
		m_sqlda_out->sqlvar[Index].sqldata = (char*) Data;
		m_sqlda_out->sqlvar[Index].sqlind = NULL;
	}
	
	bool Execute (
		Diag& diag,
		isc_tr_handle* txn,
		bool Query = false);
		
	bool Fetch (
		Diag& diag,
		bool& eof);

	void Close();
		
protected:
	
	Terminal* m_Terminal;
	isc_stmt_handle m_stmt;
	XSQLDA* m_sqlda_in;
	XSQLDA* m_sqlda_out;
};

class Terminal
{
public:

	Terminal()
	{
	}

	void Init(
		const Config* config,
		Monitor* Monitor,
		w_id_t Warehouse)
	{
		m_Monitor = Monitor;
		m_Warehouse = Warehouse;

		m_db = 0;
		m_Thread = 0;

		m_neword1_stmt.Init(this);
		m_neword2_stmt.Init(this);
		m_payment_stmt.Init(this);
		m_ostat1_stmt.Init(this);
		m_ostat2_stmt.Init(this);
		m_delivery_stmt.Init(this);
		m_slev_stmt.Init(this);
	}

	void Start();
	void Wait();

	isc_db_handle* getDb() { return &m_db; }

	void Run();

	bool check_status(
		Diag& diag, 
		ISC_STATUS* status);

protected:

	void Transaction();

	bool Prepare(
		Diag& diag);
		
	void gen_neword_input(
		new_order_input* in,	
		new_order_input_repeat* in_rpt);

	void gen_payment_input(
		payment_input* in);

	void gen_ostat_input(
		ostat_input* in);

	void gen_delivery_input(
		delivery_input* in);

	void gen_slev_input(
		slev_input* in);

	bool neword (Diag& diag, isc_tr_handle* txn, bool& rollback);
	bool payment(Diag& diag, isc_tr_handle* txn);
	bool ostat(Diag& diag, isc_tr_handle* txn);
	bool delivery(Diag& diag, isc_tr_handle* txn);
	bool slev(Diag& diag, isc_tr_handle* txn);

#ifndef _WINDOWS
	static void* _thread (void*);
#else
	static unsigned WINAPI _thread (PVOID);
#endif

	// initialization
	Monitor* m_Monitor;
	w_id_t m_Warehouse;

	// state
	isc_db_handle m_db;
	
	Stmt m_neword1_stmt;
	Stmt m_neword2_stmt;
	Stmt m_payment_stmt;
	Stmt m_ostat1_stmt;
	Stmt m_ostat2_stmt;
	Stmt m_delivery_stmt;
	Stmt m_slev_stmt;

	Deck m_Deck;
	AHandle m_Thread;
};

class Monitor
{
public:

	typedef LockGuard<Monitor> ObjectLock;

	struct TxnReport
	{
		ISC_ULONG Timestamp;
		ISC_ULONG NewOrderCount;
		
		TxnReport()
		{
			Timestamp = 0;
			NewOrderCount = 0;
		}
		
		void Init(ISC_ULONG Time)
		{
			Timestamp = Time;
			NewOrderCount = 0;
		}
	};
	
	~Monitor()
	{
		ISC_STATUS_ARRAY status = {0};
		if (m_db) {
			isc_detach_database(status, &m_db);
		}
	}

	void Init (
		const Config* config);

	void Run(
		ISC_ULONG RunTime,
		ISC_ULONG RumpUpTime,
		ISC_ULONG ReportInterval);

	bool RunInterval (
		ISC_ULONG Until,
		ISC_ULONG Interval);

	void EnterSteadyState(ISC_ULONG Time);

	bool isStopped() const { return m_isStopped; }
	bool isSteady() const { return m_isSteady; }

	void PutTransaction (
		txn_t TxnType,
		ISC_ULONG KeyingTime, 
		ISC_ULONG ResponseTime,
		ISC_ULONG ThinkTime,
		bool rollback);
		
	void RunReport(ISC_ULONG Time);
	void Stop(ISC_ULONG Time = 0);

	void ReportTotal(
		ISC_ULONG Time);
		
	void Lock() { m_Mutex.Lock(); }
	void Unlock() { m_Mutex.Unlock(); }

	ISC_ULONG getWarehouses() const { return m_Warehouses; }
	ISC_ULONG getItems() const { return m_Items; }
	ISC_ULONG getCustomers() const { return m_Customers; }
	ISC_ULONG getOrders() const { return m_Orders; }

	const char* getDatabase() const { return m_Database; }
	const char* getUser() const { return m_User; }
	const char* getPassword() const { return m_Password; }

protected:

	// initialization
	char m_Database[200];
	char m_User[200];
	char m_Password[200];
	ISC_ULONG m_Warehouses;
	ISC_ULONG m_Items;
	ISC_ULONG m_Customers;
	ISC_ULONG m_Orders;

	isc_db_handle m_db;
	long m_info_reads, m_info_writes, m_info_fetches;

	Mutex m_Mutex;
	SemHandle m_AlertEvent;
	
	ISC_ULONG m_TerminalCount;
	Terminal* m_Terminals;

	ISC_ULONG m_ReportCount;
	TxnReport* m_Reports;
	
	TxnStats m_TxnStats[_TXN_COUNT];
	
	volatile bool m_isStopped;
	volatile bool m_isSteady;

	ISC_ULONG m_StartTime;

	ISC_ULONG m_SteadyStartTime;
	ISC_ULONG m_FirstSteadyReport;
	
	ISC_ULONG m_CurrentReport;
};

bool Terminal::check_status(
	Diag& diag,
	ISC_STATUS* status)
{
	bool ok;

	ok = (status[1] == 0);
	if (!ok)
	{
		LockGuard<Mutex> lock(&printfLock);
		diagnostics(&diag, status, isc_sqlcode(status));

		switch(status[1])
		{
		case isc_shutdown:
		case isc_unavailable:
			m_Monitor->Stop();

		default:
			;
		}
	}
	return ok;
}


bool Stmt::Prepare(
	Diag& diag,
	isc_tr_handle* txn,
	const char* sql,
	ISC_ULONG in,
	ISC_ULONG out,
	short dialect)
{
	ISC_STATUS status[20];
		
	isc_dsql_alloc_statement2(
		status,
		m_Terminal->getDb(),
		&m_stmt);
	
	if (m_Terminal->check_status(diag, status))
	{
		m_sqlda_in = (XSQLDA*) malloc (XSQLDA_LENGTH(in + 1));
		memset(m_sqlda_in, 0, XSQLDA_LENGTH(in + 1));

		m_sqlda_in->sqln = in;
		m_sqlda_in->version = SQLDA_VERSION1;
		
		m_sqlda_out = (XSQLDA*) malloc (XSQLDA_LENGTH(out + 1));
		memset(m_sqlda_out, 0, XSQLDA_LENGTH(out + 1));
		
		m_sqlda_out->sqln = out;
		m_sqlda_out->version = SQLDA_VERSION1;
		
		isc_dsql_prepare(
			status,
			txn,
			&m_stmt,
			0, const_cast<char*>(sql),
			dialect,
			m_sqlda_out);
		
		if (m_Terminal->check_status(diag, status))
		{
			isc_dsql_describe_bind (
				status,
				&m_stmt,
				dialect,
				m_sqlda_in);
			
			return m_Terminal->check_status(diag, status);
		}
		
		isc_dsql_free_statement(
			status,
			&m_stmt,
			DSQL_drop);
	}
	
	return false;
}

bool Stmt::Execute(
	Diag& diag,
	isc_tr_handle* txn,
	bool Query)
{
	ISC_STATUS status[20];
	
	isc_dsql_execute2(
		status,
		txn,
		&m_stmt,
		DIALECT,
		m_sqlda_in,
		Query ? NULL : m_sqlda_out);

	return (m_Terminal->check_status(
		diag,
		status));
}

bool Stmt::Fetch (
	Diag& diag,
	bool& eof)
{
	bool ok = true;
	ISC_STATUS status[20];

	eof = (isc_dsql_fetch(
		status,
		&m_stmt,
		DIALECT,
		m_sqlda_out) == 100);

	ok = m_Terminal->check_status(diag, status);

	return ok;
}

void Stmt::Close()
{
	ISC_STATUS status[20];

	isc_dsql_free_statement(status, &m_stmt, DSQL_close);
}


void Terminal::Start()
{
#ifdef _WINDOWS
	m_Thread = (HANDLE) _beginthreadex (
		NULL,
		0,
		_thread,
		this,
		0,
		NULL);
#else
	if (pthread_create(&m_Thread, NULL, _thread, this) != 0)
		throw "pthread_create";
#endif
}

void Terminal::Wait()
{
#ifdef _WINDOWS
	::WaitForSingleObject(m_Thread, INFINITE);
#else
	pthread_join(m_Thread, 0);
#endif
}

bool Terminal::Prepare(
	Diag& diag)
{
	bool ok = true;
	ISC_STATUS status[20];
	
	static const ISC_UCHAR tpb[] = 
	{
		isc_tpb_version3,
		isc_tpb_read_committed,
		isc_tpb_no_rec_version
	};

	isc_tr_handle prepare_txn_handle = 0;
	
	isc_start_transaction(
		status, 
		&prepare_txn_handle,
		1,
		&m_db,
		lengthof(tpb),
		tpb);
	
	ok = check_status(diag, status);

	if (ok)
	{
		ok &= m_neword1_stmt.Prepare(
			diag,
			&prepare_txn_handle,
			"execute procedure neword1 (?,?,?,?,?)",
			5, 7, DIALECT);

		ok &= m_neword2_stmt.Prepare(
			diag,
			&prepare_txn_handle,
			"execute procedure neword2 (?,?,?,?,?,?,?)",
			7, 6, DIALECT);
		
		ok &= m_payment_stmt.Prepare(
			diag,
			&prepare_txn_handle,
			"execute procedure payment (?,?,?,?,?,?,?)",
			7, 29, DIALECT);
		
		ok &= m_ostat1_stmt.Prepare(
			diag,
			&prepare_txn_handle,
			"execute procedure ostat1(?,?,?,?)",
			4, 8, DIALECT);
		
		ok &= m_ostat2_stmt.Prepare(
			diag,
			&prepare_txn_handle,
			"\
			   SELECT ol_i_id, ol_supply_w_id, ol_quantity, \
			   ol_amount, ol_delivery_d \
			   FROM order_line \
			   WHERE ol_w_id=? AND ol_d_id=? AND ol_o_id=?",
			   3, 5, DIALECT);
		
		ok &= m_delivery_stmt.Prepare(
			diag,
			&prepare_txn_handle,
			"execute procedure delivery(?,?)",
			2, 0, DIALECT);

		ok &= m_slev_stmt.Prepare(
			diag,
			&prepare_txn_handle,
			"execute procedure slev(?,?,?)",
			3, 1, DIALECT);
		
		isc_commit_transaction(
			status,
			&prepare_txn_handle);
	}
	
	return ok;
}


void Terminal::Run()
{
	Diag diag;

	ISC_STATUS status[20];

	seed(TimeNow());

	v_printf("Terminal (thread %d) on warehouse %d started ...\n", 
#ifdef _WINDOWS
		::GetCurrentThreadId(),
#else
		pthread_self(),
#endif
		m_Warehouse);

	char dpb[1024];
	int index = 0;
	memset(dpb, 0, 1024);

	dpb[index++] = 1;
	const char *user = m_Monitor->getUser();
	if (size_t len = strlen(user))
	{
		dpb[index++] = isc_dpb_user_name;
		dpb[index++] = len;
		memcpy(dpb + index, user, len);
		index += len;
	}
	const char *password = m_Monitor->getPassword();
	if (size_t len = strlen(password))
	{
		dpb[index++] = isc_dpb_password;
		dpb[index++] = len;
		memcpy(dpb + index, password, len);
		index += len;
	}
	
	{
		// isc_attach_db serialization
		Monitor::ObjectLock lock(m_Monitor);
		
		isc_attach_database(
			status, 
			0, (char*) m_Monitor->getDatabase(), 
			&m_db, 
			index, dpb);
	}

	if (check_status(diag, status))
	{
		char info[] = {isc_info_attachment_id, isc_info_end};
		char buff[32];
		isc_database_info(status, &m_db, sizeof(info), info, sizeof(buff), buff);

		char *p = buff;
		p++;
		int l = isc_vax_integer(p, 2);
		p += 2;
		l = isc_vax_integer(p, l);

		if (Prepare(diag))
		{
			while (!m_Monitor->isStopped())
				Transaction();
		}
		else
		{
			v_printf("Terminal %d (thread %d) failed to prepare queries, %s\n",
				m_Warehouse, 
#ifdef _WINDOWS
				::GetCurrentThreadId(),
#else
				pthread_self(),
#endif
				diag.message);

			m_Monitor->Stop();
		}
		isc_detach_database(status, &m_db);
	}
	else
	{
		v_printf("Terminal %d (thread %d) failed to connect to the database '%s', %s\n",
			m_Warehouse, 
#ifdef _WINDOWS
			::GetCurrentThreadId(),
#else
			pthread_self(),
#endif
			m_Monitor->getDatabase(),
			diag.message);

		m_Monitor->Stop();
	}
}

void Terminal::Transaction()
{
	Diag diag;
	
	ISC_ULONG StartTime = TimeNow();

	txn_t txn_type = m_Deck.Draw();
	ISC_ULONG KeyingTime = txn_data[txn_type].KeyingTime;
	
#ifdef TPCC_TIMED
	Sleep(KeyingTime / TPCC_TIMING);
#endif

	ISC_ULONG SendTime = TimeNow();

	bool ok = true;
	ISC_STATUS status[20];

	static const ISC_UCHAR tpb[] = 
	{
		isc_tpb_version3,
		isc_tpb_read_committed,
		isc_tpb_no_rec_version
//		isc_tpb_concurrency
	};
	
	isc_tr_handle txn = 0;
	
	isc_start_transaction(
		status, 
		&txn,
		1,
		&m_db,
		lengthof(tpb),
		tpb);
	
	ok = check_status(diag, status);

	if (ok)
	{
		bool rollback = false;

		switch (txn_type)
		{
		case NEW_ORDER:
			ok = neword(diag, &txn, rollback);
			break;
		case PAYMENT:
			ok = payment(diag, &txn);
			break;
		case ORDER_STATUS:
			ok = ostat(diag, &txn);
			break;
		case DELIVERY:
			ok = delivery(diag, &txn);
			break;
		case STOCK_LEVEL:
			ok = slev(diag, &txn);
			break;
		default:
			assert(!"Invalid transaction type.");
		}

		if (!ok || rollback)
		{
			// rollback = true;

			isc_rollback_transaction(
				status,
				&txn);

			ok &= check_status(diag, status);
		}
		else
		{
			isc_commit_transaction(
				status,
				&txn);

			ok &= check_status(diag, status);
		}

		ISC_ULONG ResponseTime = TimeNow();

#ifdef TPCC_TIMED
		Sleep(get_think_time(txn_type) / TPCC_TIMING);
#endif //TPCC_TIMED

		ISC_ULONG EndTime = TimeNow();

		if (ok)
		{
			m_Monitor->PutTransaction(
				txn_type, 
				SendTime - StartTime, 
				ResponseTime - SendTime,
				EndTime - ResponseTime,
				rollback);
		}
	}

	if (!ok)
	{
		v_printf("Terminal %d (thread %d) '%s' transaction failure, %s\n",
			m_Warehouse, 
#ifdef _WINDOWS
			::GetCurrentThreadId(),
#else
			pthread_self(),
#endif
			txn_data[txn_type].Name,
			diag.message);
	}
}

#ifdef _WINDOWS
unsigned Terminal::_thread (PVOID arg)
#else
void* Terminal::_thread (void* arg)
#endif
{
	Terminal* terminal = (Terminal*) arg;
	terminal->Run();
	return 0;
}

#define MAXITEMS 100000
#define MAXITEMS_SCALE_DOWN 100
#define CUSTOMERS_PER_DISTRICT 3000
#define CUSTOMERS_SCALE_DOWN 30
#define ORDERS_PER_DISTRICT 3000
#define ORDERS_SCALE_DOWN 30

void Monitor::Init (
	const Config* config)
{
	ISC_STATUS_ARRAY status = {0};

	strcpy(m_Database, config->Database);
	strcpy(m_User, config->User);
	strcpy(m_Password, config->Password);

	char dpb[1024];
	int index = 0;
	memset(dpb, 0, 1024);

	dpb[index++] = 1;
	const char *user = m_User;
	if (size_t len = strlen(user))
	{
		dpb[index++] = isc_dpb_user_name;
		dpb[index++] = len;
		memcpy(dpb + index, user, len);
		index += len;
	}
	const char *password = m_Password;
	if (size_t len = strlen(password))
	{
		dpb[index++] = isc_dpb_password;
		dpb[index++] = len;
		memcpy(dpb + index, password, len);
		index += len;
	}

	m_db = 0;
	isc_attach_database(
		status, 
		0, 
		m_Database, 
		&m_db, 
		index,
		dpb);
	m_info_reads = m_info_writes = m_info_fetches = 0;

#ifdef _WINDOWS
	m_AlertEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
#else
	sem_init(&m_AlertEvent, 0, 0);
#endif

	m_Warehouses = config->Warehouses;
	m_Items         = config->ScaleDown ? MAXITEMS_SCALE_DOWN : MAXITEMS;
	m_Customers = config->ScaleDown ? CUSTOMERS_SCALE_DOWN : CUSTOMERS_PER_DISTRICT;
	m_Orders        = config->ScaleDown ? ORDERS_SCALE_DOWN : ORDERS_PER_DISTRICT;

	m_TerminalCount = config->Terminals;
	m_Terminals = new Terminal [m_TerminalCount];

	for (ISC_ULONG i = 0; i < m_TerminalCount; ++i)
	{
		m_Terminals[i].Init(config, this, ((i + config->StartingWarehouse - 1) % m_Warehouses) + 1);
	}

	for (ISC_ULONG i = 0; i < _TXN_COUNT; ++i)
	{
		m_TxnStats[i].Init(
			config->ResponseTimeIntervals,
			config->ResponseTimeLimit,
			config->ThinkTimeIntervals,
			config->ThinkTimeLimit);
	}
	
	m_ReportCount = (ISC_ULONG) (config->RunTime / config->ReportInterval) + 1;
	m_Reports = new TxnReport [m_ReportCount];
	m_FirstSteadyReport = m_CurrentReport = 0;
}

void Monitor::PutTransaction(
	txn_t TxnType,
	ISC_ULONG KeyingTime, 
	ISC_ULONG ResponseTime,
	ISC_ULONG ThinkTime,
	bool rollback)
{
	ObjectLock lock(this);

	if (isStopped())
		return;

	m_TxnStats[TxnType].PutTransaction(
		KeyingTime, 
		ResponseTime,
		ThinkTime,
		rollback);
	
	if (TxnType == NEW_ORDER)
	{
		if (m_CurrentReport < m_ReportCount)
		{
			m_Reports[m_CurrentReport].NewOrderCount ++;
		}
		else
		{
			assert(!"Out of reports");
		}
	}
}

void Monitor::RunReport(
	ISC_ULONG Time)
{
	double tpm;
	ISC_ULONG Interval;
	
	char items[] = {
		isc_info_reads, 
		isc_info_writes, 
		isc_info_fetches
	};

	char buffer[sizeof(items) * 7 + 2];

	{
		ObjectLock lock(this);

		Interval = m_CurrentReport;
		TxnReport& report = m_Reports[m_CurrentReport++];

		tpm = (double) report.NewOrderCount / (Time - report.Timestamp) * TIME_SCALE * 60;

		m_Reports[m_CurrentReport].Init(Time);

		ISC_STATUS_ARRAY status;
		isc_database_info(status, &m_db, 
			sizeof(items), items,
			sizeof(buffer), buffer);
	}

	long info_reads = 0, info_writes = 0, info_fetches = 0;
	char *p = buffer, *const e = buffer + sizeof(buffer); 
	while(p < e)
	{
		int len;
		switch(*p)
		{
		case isc_info_reads:
			len = isc_vax_integer(p+1, 2);
			info_reads = isc_vax_integer(p+1+2, len);
			p += len + 3;
			break;

		case isc_info_writes:
			len = isc_vax_integer(p+1, 2);
			info_writes = isc_vax_integer(p+1+2, len);
			p += len + 3;
			break;

		case isc_info_fetches:
			len = isc_vax_integer(p+1, 2);
			info_fetches = isc_vax_integer(p+1+2, len);
			p += len + 3;
			break;

		default:
			p = e;
		}
	}

	v_printf("Measurement point %4d, time = %7.2f, MQTh = %8.2f, f = %9lu, r = %6lu, w = %6lu\n",
		Interval,
		(double) (Time - m_StartTime) / TIME_SCALE,
		tpm, 
		info_fetches - m_info_fetches,
		info_reads - m_info_reads, 
		info_writes - m_info_writes);

	m_info_fetches = info_fetches;
	m_info_reads = info_reads;
	m_info_writes = info_writes;
}

bool Monitor::RunInterval(
	ISC_ULONG Until,
	ISC_ULONG Interval)
{
	if (isStopped())
		return false;

	ISC_ULONG Time = TimeNow();
	
	for (;;)
	{
		if (Time >= Until)
			return true;

#ifdef _WINDOWS
		DWORD WaitStatus = ::WaitForSingleObject(m_AlertEvent, Interval);
#else
		// Wait with timeout
		timespec timeout = getCurrentTime();
		timeout.tv_sec += Interval / 1000;
		timeout.tv_nsec += (Interval % 1000) * 1000000;
		timeout.tv_sec += (timeout.tv_nsec / 1000000000l);
		timeout.tv_nsec %= 1000000000l;
		int WaitStatus = sem_timedwait(&m_AlertEvent, &timeout);
#endif

		Time = TimeNow();

		RunReport(Time);

		if (WaitStatus == WAIT_OBJECT_0)
			return false;
	}
}

void Monitor::EnterSteadyState(
	ISC_ULONG Time)
{
	if (isStopped())
		return;

	v_printf("Entering measurement interval ...\n");

	ObjectLock lock (this);

	m_isSteady = true;
	m_FirstSteadyReport = m_CurrentReport;
	m_SteadyStartTime = Time;

	for (TxnStats* p = m_TxnStats; p != m_TxnStats + _TXN_COUNT; ++p)
	{
		p->Reset();
	}
}

void Monitor::Run(
	ISC_ULONG RunTime,
	ISC_ULONG RumpUpTime,
	ISC_ULONG ReportInterval)
{
	ISC_ULONG i;

	m_StartTime = TimeNow();
	m_isStopped = false;
	m_isSteady = false;
	m_Reports[0].Init(m_StartTime);
	m_CurrentReport = 0;

	v_printf("Starting terminals ...\n");

	for (i = 0; i < m_TerminalCount; ++i)
	{
		m_Terminals[i].Start();
	}

#ifdef _WINDOWS
	::SetThreadPriority (::GetCurrentThread(), THREAD_PRIORITY_HIGHEST);
#else
	// TODO: for linux
#endif
	
	v_printf("Entering rump-up period (for %d minutes) ...\n", 
		RumpUpTime / (60 * TIME_SCALE));

	RunInterval(m_StartTime + RumpUpTime, ReportInterval);

	EnterSteadyState(TimeNow());

	RunInterval(m_StartTime + RunTime, ReportInterval);

	ISC_ULONG EndTime = TimeNow();

	Stop(EndTime);

	v_printf("Waiting terminals to shut down...\n");

	for (i = 0; i < m_TerminalCount; ++i)
	{
		m_Terminals[i].Wait();
	}

	ReportTotal(EndTime);
}

void Monitor::Stop(ISC_ULONG EndTime)
{
	if (m_isStopped)
		return;

	if (!EndTime)
		EndTime = TimeNow();


	v_printf("Test run ended, time %.2f.\n",
		(double) (EndTime - m_StartTime) / TIME_SCALE);

	m_isStopped = true;

#ifdef _WINDOWS
	SetEvent(m_AlertEvent);
#else
	sem_post(&m_AlertEvent);
#endif
}

void Monitor::ReportTotal(
	ISC_ULONG Time)
{
	// overall terformance
	// last (in-progress) report is not counted

	ISC_ULONG SumTxn = 0;
	for (int i = m_FirstSteadyReport; i != m_CurrentReport; ++i)
	{
		SumTxn += m_Reports[i].NewOrderCount;
	}

	double tpmC = (double) SumTxn / (Time - m_SteadyStartTime) * TIME_SCALE * 60;

	ISC_ULONG TotalTxnCount = 0;
	for (ISC_ULONG txn = 0; txn < _TXN_COUNT; ++txn)
	{
		TotalTxnCount += m_TxnStats[txn].getCount();
	}

	v_printf("TPC-C Throughput: %.2f tpmC\n", tpmC);
	v_printf("Transactions (all types) completed during measurement interval: %d\n", TotalTxnCount);
	v_printf("Fetches = %10lu, Reads = %6lu, Writes = %6lu\n", 
		m_info_fetches,
		m_info_reads, 
		m_info_writes);
	
	v_printf("\nNumerical Quantities Summary\n");

	v_printf("\nTransaction Mix, in percent of total transactions\n");
	v_printf("Transaction       Count    Percent Rollback\n");
	for (ISC_ULONG txn = 0; txn < _TXN_COUNT; ++txn)
	{
		ISC_ULONG Count = m_TxnStats[txn].getCount();
		ISC_ULONG rollback = m_TxnStats[txn].getRolledbackCount();

		v_printf("%-16s %6d    %7.2f %8d\n",
			txn_data[txn].Name,
			Count,
			(double) Count / TotalTxnCount * 100,
			rollback);
	}
	
	v_printf("\nResponse Times, in seconds\n");
	v_printf("Transaction        90th %%ile     Minimum     Average     Maximum\n");
	for (ISC_ULONG txn = 0; txn < _TXN_COUNT; ++txn)
	{
		const Stats& stats = m_TxnStats[txn].getResponse();
		ISC_ULONG Count = m_TxnStats[txn].getCount();
		
		v_printf("%-16s %11.3f %11.3f %11.3f %11.3f\n",
			txn_data[txn].Name,
			(double) stats.getPercentile(0.9, Count) * 100.0,
			(double) stats.getMin() / TIME_SCALE,
			(double) stats.getTotal() / Count / TIME_SCALE,
			(double) stats.getMax() / TIME_SCALE);
	}

	ISC_ULONG MaxResponseCount = 0;
	for (ISC_ULONG txn = 0; txn < _TXN_COUNT; ++txn)
	{
		const Stats& stats = m_TxnStats[txn].getResponse();
		
		if (stats.getIntervalCount() > MaxResponseCount)
			MaxResponseCount = stats.getIntervalCount();
	}
	
	v_printf("\nResponse Time Histogram, in percent\n");
	v_printf("Measurement      NO       P      OS       D     SL5\n");
	for (ISC_ULONG i = 0; i < MaxResponseCount; ++i)
	{
		v_printf("%11d", i);

		for (ISC_ULONG txn = 0; txn < _TXN_COUNT; ++txn)
		{
			const Stats& stats = m_TxnStats[txn].getResponse();
			ISC_ULONG Count = m_TxnStats[txn].getCount();
			
			if (i < stats.getIntervalCount())
				v_printf(" %7.3f", (double) stats.getValue(i) / Count * 100.0);
			else
				v_printf("        ");
		}
	
		v_printf("\n");
	}
	
	v_printf("\nKeying Times, in seconds\n");
	v_printf("Transaction	Average	Minimum	Maximum\n");
	for (ISC_ULONG txn = 0; txn < _TXN_COUNT; ++txn)
	{
		const Stats& stats = m_TxnStats[txn].getKeying();
		ISC_ULONG Count = m_TxnStats[txn].getCount();
		
		v_printf("%s	%.2f	%.2f	%.2f\n", 
			txn_data[txn].Name,
			(double) stats.getMin() / TIME_SCALE,
			(double) stats.getTotal() / Count / TIME_SCALE,
			(double) stats.getMax() / TIME_SCALE);
	}
	
	v_printf("\nThink Times, in seconds\n");
	v_printf("Transaction	Average	Minimum	Maximum\n");
	for (ISC_ULONG txn = 0; txn < _TXN_COUNT; ++txn)
	{
		const Stats& stats = m_TxnStats[txn].getThink();
		ISC_ULONG Count = m_TxnStats[txn].getCount();
		
		v_printf("%s	%.2f	%.2f	%.2f\n", 
			txn_data[txn].Name,
			(double) stats.getMin() / TIME_SCALE,
			(double) stats.getTotal() / Count / TIME_SCALE,
			(double) stats.getMax() / TIME_SCALE);
	}
}


void Usage ()
{
	v_printf("TPCC Firebird:\n\n");
	v_printf("Parameter (Default)\n");
	v_printf("--------------------------------------------------------------\n");
	v_printf("-W Number of Warehouses to Load (%d)\n", DEF_WAREHOUSES);
	v_printf("-w Staring Warehouse (%d)\n", DEF_STARING_WAREHOUSE);
	v_printf("-T Number of terminals (%d)\n", DEF_TERMINALS);
	v_printf("-D Database %s\n", DEF_DATABASE);
	v_printf("-U User %s\n", DEF_USER);
	v_printf("-P Password %s\n", DEF_PASSWORD);
	v_printf("-c Scaled Database (normal = 0, tiny = 1) %ld\n", 0);
	v_printf("-R test run duration, min (%d)\n", DEF_RUN_TIME);
	v_printf("-r rump-up duration, min (%d)\n", DEF_RUMP_UP_TIME);
	v_printf("-i report interval, sec (%d)\n", DEF_REPORT_INTERVAL);
	v_printf("-a response time graph steps (%d)\n", DEF_RESPONSE_TIME_INTERVALS);
	v_printf("-x response time graph maximum, sec (%d)\n", DEF_RESPONSE_TIME_LIMIT);
	v_printf("-b think time graph steps (%d)\n", DEF_THINK_TIME_INTERVALS);
	v_printf("-y think time graph maximum, sec (%d)\n", DEF_THINK_TIME_LIMIT);
	v_printf("\nNote: Command line switches are case sensitive.\n");
}

void Config::Init (
	int argc, 
	char ** argv)
{
	const char *ptr;

	memset(this, 0, sizeof(*this));

	strcpy(Database, DEF_DATABASE);
	strcpy(User, DEF_PASSWORD);
	strcpy(Password, DEF_PASSWORD);
	Warehouses = DEF_WAREHOUSES;
	StartingWarehouse = DEF_STARING_WAREHOUSE;
	Terminals = DEF_TERMINALS;
	RunTime = DEF_RUN_TIME * 60 * TIME_SCALE;
	RumpUpTime = DEF_RUMP_UP_TIME * 60 * TIME_SCALE;
	ResponseTimeIntervals = DEF_RESPONSE_TIME_INTERVALS;
	ResponseTimeLimit = DEF_RESPONSE_TIME_LIMIT * TIME_SCALE;
	ThinkTimeIntervals = DEF_THINK_TIME_INTERVALS;
	ThinkTimeLimit = DEF_THINK_TIME_LIMIT * TIME_SCALE;
	ReportInterval = DEF_REPORT_INTERVAL * TIME_SCALE;
	ScaleDown = false;

	for (int i = 1; i < argc; ++i)
	{
		if (argv[i][0] != '-' && argv[i][0] != '/')
		{
			v_printf("Unrecognized command\n");
			Usage();
			exit(1);
		}

		ptr = argv[i];
		switch (ptr[1])
		{
		case 'h': /* Fall throught */
		case 'H':
			Usage();
			break;
		case 'D':
			strcpy(Database, ptr + 2);
			break;
		case 'U':
			strcpy(User, ptr + 2);
			break;
		case 'P':
			strcpy(Password, ptr + 2);
			break;
		case 'T':
			Terminals = atoi(ptr + 2);
			break;
		case 'W':
			Warehouses = atol(ptr + 2);
			break;
		case 'c':
			ScaleDown = (atol(ptr + 2) != 0);
			break;
		case 'R':
			RunTime = atoi(ptr + 2) * 60 * TIME_SCALE;
			break;
		case 'r':
			RumpUpTime = atoi(ptr + 2) * 60 * TIME_SCALE;
			break;
		case 'i':
			ReportInterval = atol(ptr + 2) * TIME_SCALE;
			break;
		case 'a':
			ResponseTimeIntervals = atol(ptr + 2);
			break;
		case 'x':
			ResponseTimeLimit = atol(ptr + 2) * TIME_SCALE;
			break;
		case 'b':
			ThinkTimeIntervals = atol(ptr + 2);
			break;
		case 'y':
			ThinkTimeLimit = atol(ptr + 2) * TIME_SCALE;
			break;
		default:
			Usage();
			exit(-1);
			break;
		}
	}
}

int main(int argc, char** argv)
{
	TimeInit();
	seed(TimeNow());

	if (argc < 2)
	{
		Usage();
		exit(-1);
	}

//#ifdef _DEBUG
	//DebugBreak();

	const char* p = getenv("ISC_PASSWORD");
	if (!p || !*p)
	{
		putenv((char *)"ISC_USER=SYSDBA");
		putenv((char *)"ISC_PASSWORD=masterkey");
	}
//#endif

	Config config;
	config.Init(argc, argv);

	Monitor monitor;
	monitor.Init(&config);

	monitor.Run(
		config.RunTime,
		config.RumpUpTime,
		config.ReportInterval);

	return 0;
}


int URandExcept(int x, int y, int c)
{
	int r;

	if (x == y)
		r = x;
	else
	{
		r = RandomNumber(x, y - 1);
		if (r >= c)
			r++;
	}

	return r;
}

#define MAXITEMS 100000
#define MAXITEMS_SCALE_DOWN 100
#define CUSTOMERS_PER_DISTRICT 3000
#define CUSTOMERS_SCALE_DOWN 30
#define ORDERS_PER_DISTRICT 3000
#define ORDERS_SCALE_DOWN 30


#define NURAND_C_C_ID 151
#define NURAND_C_C_LAST 151
#define NURAND_C_OL_I_ID 151

/*TPC Benchmark<tm> C - Standard Specification, Revision 5.0 - Page 29 of 130
2.4.2.3 For transactions that rollback as a result of an unused item number, the complete transaction profile
must be executed with the exception that the following steps need not be done:
- Selecting and retrieving the row in the STOCK table with S_I_ID matching the unused item number.
- Examining the strings I_DATA and S_DATA for the unused item.
- Inserting a new row into the ORDER-LINE table for the unused item.
- Adding the amount for the unused item to the sum of all OL_AMOUNT.
The transaction is not committed. Instead, the transaction is rolled back.
Comment 1: The intent of this clause is to ensure that within the New-Order transaction all valid items are
processed prior to processing the unused item. Knowledge that an item is unused, resulting in rolling back the
transaction, can only be used to skip execution of the above steps. No other optimization can result from this
knowledge (e.g., skipping other steps, changing the execution of other steps, using a different type of transaction,
etc.)..Comment 2: This clause is an exception to Clause 2.3.1. The order of data manipulations prior to signaling a "not
found" condition is immaterial.
*/

void Terminal::gen_neword_input(
	new_order_input* in,	
	new_order_input_repeat* in_rpt)
{
	bool_t rbk;
	int i;

	in->d_id = RandomNumber(1, 10);
	in->c_id = NURand(1023, 1, m_Monitor->getCustomers(), NURAND_C_C_ID);
	in->o_ol_cnt = RandomNumber(5, 15);
	rbk = (RandomNumber(1, 100) == 1);

	for (i = 0; i < in->o_ol_cnt; ++i, ++in_rpt)
	{
		if (rbk && i == in->o_ol_cnt - 1)
			in_rpt->ol_i_id = -1;
		else
			in_rpt->ol_i_id = NURand(8191, 1, m_Monitor->getItems(), NURAND_C_OL_I_ID);

		if (m_Monitor->getWarehouses() == 1
			|| RandomNumber(1, 100) != 1)
		{
			in_rpt->ol_supply_w_id = m_Warehouse;
		}
		else
		{
			in_rpt->ol_supply_w_id = URandExcept(1, m_Monitor->getWarehouses(), m_Warehouse);
		}

		in_rpt->ol_quantity = RandomNumber(1, 10);
	}
}

bool Terminal::neword (
	Diag& diag,
	isc_tr_handle* txn,
	bool& rollback)
{
	bool ok = true;

	new_order_input in;	
	new_order_input_repeat in_rpt[MAX_ORDER_ITEMS];
	new_order_output out;	
	new_order_output_repeat out_rpt[MAX_ORDER_ITEMS];
	
	gen_neword_input(&in, in_rpt);

	rollback = false;

	ISC_SHORT ol_number;

	ISC_SHORT in_o_all_local = 1;
	for (ol_number = 0; ol_number < in.o_ol_cnt; ol_number++)
	{
		if (in_rpt[ol_number].ol_supply_w_id != m_Warehouse) 
		{
			in_o_all_local = 0;
			break;
		}
	}

	m_neword1_stmt.BindInput(0, SQL_SHORT, 2, &m_Warehouse);
	m_neword1_stmt.BindInput(1, SQL_SHORT, 2, &in.d_id);
	m_neword1_stmt.BindInput(2, SQL_SHORT, 2, &in.c_id);
	m_neword1_stmt.BindInput(3, SQL_SHORT, 2, &in.o_ol_cnt);
	m_neword1_stmt.BindInput(4, SQL_SHORT, 2, &in_o_all_local);
	
	m_neword1_stmt.BindOutput(0, SQL_LONG, 4, &out.o_id);
	m_neword1_stmt.BindOutput(1, SQL_TEXT, 16, &out.c_last);
	m_neword1_stmt.BindOutput(2, SQL_TEXT, 2, &out.c_credit);
	m_neword1_stmt.BindOutput(3, SQL_LONG, 4, &out.c_discount);
	m_neword1_stmt.BindOutput(4, SQL_LONG, 4, &out.w_tax);
	m_neword1_stmt.BindOutput(5, SQL_LONG, 4, &out.d_tax);
	m_neword1_stmt.BindOutput(6, SQL_TIMESTAMP, 8, &out.o_entry_d);
	
	ok = m_neword1_stmt.Execute(
		diag,
		txn);

	if (ok)
	{
		out.total_amount = 0;
		
		for (ol_number = 1; ol_number <= in.o_ol_cnt; ol_number++)
		{
			const new_order_input_repeat* in_r = &in_rpt[ol_number - 1];
			new_order_output_repeat* out_r = &out_rpt[ol_number - 1];
			
			ISC_SHORT item_not_found/* = 0*/;
			
			m_neword2_stmt.BindInput(0, SQL_SHORT, 2, &m_Warehouse);
			m_neword2_stmt.BindInput(1, SQL_SHORT, 2, &in.d_id);
			m_neword2_stmt.BindInput(2, SQL_LONG, 4, &out.o_id);
			m_neword2_stmt.BindInput(3, SQL_SHORT, 2, &ol_number);
			m_neword2_stmt.BindInput(4, SQL_LONG, 4, &in_r->ol_i_id);
			m_neword2_stmt.BindInput(5, SQL_SHORT, 2, &in_r->ol_supply_w_id);
			m_neword2_stmt.BindInput(6, SQL_LONG, 4, &in_r->ol_quantity);
			
			m_neword2_stmt.BindOutput(0, SQL_SHORT, 2, &item_not_found);
			m_neword2_stmt.BindOutput(1, SQL_TEXT, 24, &out_r->i_name);
			m_neword2_stmt.BindOutput(2, SQL_SHORT, 2, &out_r->s_quantity);
			m_neword2_stmt.BindOutput(3, SQL_TEXT, 1, &out_r->brand_generic);
			m_neword2_stmt.BindOutput(4, SQL_LONG, 4, &out_r->i_price);
			m_neword2_stmt.BindOutput(5, SQL_INT64, 8, &out_r->ol_amount);
			
			if (!m_neword2_stmt.Execute(diag, txn))
			{
				ok = false;
				break;
			}

			if (item_not_found)
			{
				rollback = true;
				break;
			}
			
			out.total_amount += out_r->ol_amount;
			
		} /* End Order Lines */
		
		out.total_amount *= (PERCENT_SCALE + out.w_tax + out.d_tax) * (PERCENT_SCALE - out.c_discount) / PERCENT_SCALE;
		out.total_amount /= PERCENT_SCALE;
	}		

	return ok;
}

void Terminal::gen_payment_input(
	payment_input* in)
{
	int x, y;

	x = RandomNumber(1, 100);
	y = RandomNumber(1, 100);

	in->d_id = RandomNumber(1, 10);
	if (x <= 85)
	{
		in->c_d_id = in->d_id;
		in->c_w_id = m_Warehouse;
	}
	else
	{
		in->c_d_id = URandExcept(1, 10, in->d_id);
		in->c_w_id = URandExcept(1, m_Monitor->getWarehouses(), m_Warehouse);
	}

	if (y <= 60)
	{
		in->c_id = 0;
		LastName(NURand(255, 0, 999, NURAND_C_C_LAST), in->c_last);
	}
	else
	{
		InitString(in->c_last, MAX_LAST_NAME_LEN);
		in->c_id = NURand(1023, 1, m_Monitor->getCustomers(), NURAND_C_C_ID);
	}


	in->h_amount = RandomNumber(100, 500000); /* 0.01 scale */
}

void Terminal::gen_ostat_input(
	ostat_input* in)
{
	in->d_id = RandomNumber(1, 10);
	if (RandomNumber(1, 100) <= 60)
	{
		LastName(NURand(255, 0, 999, NURAND_C_C_LAST), in->c_last);
		in->c_id = 0;
	}
	else
	{
		InitString(in->c_last, MAX_LAST_NAME_LEN);
		in->c_id = NURand(1023, 1, m_Monitor->getCustomers(), NURAND_C_C_ID);
	}
}

void Terminal::gen_delivery_input(
	delivery_input* in)
{
	in->o_carrier_id = RandomNumber(1, 10);
}

void Terminal::gen_slev_input(
	slev_input* in)
{
	in->d_id = (m_Warehouse - 1) % 10 + 1;
	in->threshold = RandomNumber(10, 20);
}

/* cards */

bool Terminal::payment(
	Diag& diag,
	isc_tr_handle* txn)
{
	bool ok = true;

	payment_input in;	
	payment_output out;	

	gen_payment_input(&in);

	m_payment_stmt.BindInput(0, SQL_SHORT, 2, &m_Warehouse);
	m_payment_stmt.BindInput(1, SQL_SHORT, 2, &in.d_id);
	m_payment_stmt.BindInput(2, SQL_SHORT, 2, &in.c_w_id);
	m_payment_stmt.BindInput(3, SQL_SHORT, 2, &in.c_d_id);
	m_payment_stmt.BindInput(4, SQL_LONG, 4, &in.c_id);
	m_payment_stmt.BindInput(5, SQL_TEXT, 16, &in.c_last);
	m_payment_stmt.BindInput(6, SQL_INT64, 8, &in.h_amount);

	int i = 0;

	m_payment_stmt.BindOutput(  i, SQL_TIMESTAMP, 8, &out.h_date);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 10, &out.w_name);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 20, &out.w_street_1);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 20, &out.w_street_2);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 20, &out.w_city);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 2, &out.w_state);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 9, &out.w_zip);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 10, &out.d_name);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 20, &out.d_street_1);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 20, &out.d_street_2);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 20, &out.d_city);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 2, &out.d_state);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 9, &out.d_zip);
	m_payment_stmt.BindOutput(++i, SQL_LONG, 4, &out.c_id);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 16, &out.c_first);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 2, &out.c_middle);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 16, &out.c_last);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 20, &out.c_street_1);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 20, &out.c_street_2);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 20, &out.c_city);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 2, &out.c_state);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 9, &out.c_zip);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 16, &out.c_phone);
	m_payment_stmt.BindOutput(++i, SQL_TIMESTAMP, 8, &out.c_since); // hvlad: was 16
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 2, &out.c_credit);
	m_payment_stmt.BindOutput(++i, SQL_INT64, 8, &out.c_credit_lim);
	m_payment_stmt.BindOutput(++i, SQL_SHORT, 2, &out.c_discount);
	m_payment_stmt.BindOutput(++i, SQL_INT64, 8, &out.c_balance);
	m_payment_stmt.BindOutput(++i, SQL_TEXT, 500, &out.c_data);
	
	ok = m_payment_stmt.Execute(
		diag,
		txn);

	return ok;
}

bool Terminal::ostat(
	Diag& diag,
	isc_tr_handle* txn)
{
	bool ok = true;
	int i;

	ostat_input in;	
	ostat_output out;	
	ostat_output_repeat out_rpt[MAX_ORDER_ITEMS];

	gen_ostat_input(&in);

	m_ostat1_stmt.BindInput(i=0, SQL_SHORT, 2, &m_Warehouse);
	m_ostat1_stmt.BindInput(++i, SQL_SHORT, 2, &in.d_id);
	m_ostat1_stmt.BindInput(++i, SQL_LONG, 4, &in.c_id);
	m_ostat1_stmt.BindInput(++i, SQL_TEXT, 16, &in.c_last);

	m_ostat1_stmt.BindOutput(i=0, SQL_LONG, 4, &out.c_id);
	m_ostat1_stmt.BindOutput(++i, SQL_TEXT, 16, &out.c_first);
	m_ostat1_stmt.BindOutput(++i, SQL_TEXT, 2, &out.c_middle);
	m_ostat1_stmt.BindOutput(++i, SQL_TEXT, 16, &out.c_last);
	m_ostat1_stmt.BindOutput(++i, SQL_INT64, 8, &out.c_balance);
	m_ostat1_stmt.BindOutput(++i, SQL_LONG, 4, &out.o_id);
	m_ostat1_stmt.BindOutput(++i, SQL_TIMESTAMP, 8, &out.o_entry_d);
	m_ostat1_stmt.BindOutput(++i, SQL_SHORT, 2, &out.o_carrier_id);

	ok = m_ostat1_stmt.Execute(diag, txn);

	if (ok)
	{
		m_ostat2_stmt.BindInput(i=0, SQL_SHORT, 2, &m_Warehouse);
		m_ostat2_stmt.BindInput(++i, SQL_SHORT, 2, &in.d_id);
		m_ostat2_stmt.BindInput(++i, SQL_LONG, 4, &out.o_id);
		
		ok = m_ostat2_stmt.Execute(diag, txn, true);

		if (ok)
		{
			for (int ol = 0; ol < MAX_ORDER_ITEMS; ++ol)
			{
				ostat_output_repeat* out_r = &out_rpt[ol];

				m_ostat2_stmt.BindOutput(i=0, SQL_LONG, 4, &out_r->ol_i_id);
				m_ostat2_stmt.BindOutput(++i, SQL_SHORT, 2, &out_r->ol_supply_w_id);
				m_ostat2_stmt.BindOutput(++i, SQL_SHORT, 2, &out_r->ol_quantity);
				m_ostat2_stmt.BindOutput(++i, SQL_INT64, 8, &out_r->ol_amount);
				m_ostat2_stmt.BindOutput(++i, SQL_TIMESTAMP, 8, &out_r->ol_delivery_d);

				bool eof;
				ok = m_ostat2_stmt.Fetch(diag, eof);

				if (!ok || eof)
					break;
			}
			m_ostat2_stmt.Close();
		}
	}

	return ok;
}

bool Terminal::delivery(
	Diag& diag,
	isc_tr_handle* txn)
{
	delivery_input in;	

	gen_delivery_input(&in);

	m_delivery_stmt.BindInput(0, SQL_SHORT, 2, &m_Warehouse);
	m_delivery_stmt.BindInput(1, SQL_SHORT, 2, &in.o_carrier_id);
	
	return m_delivery_stmt.Execute(diag, txn);
}

bool Terminal::slev(
	Diag& diag,
	isc_tr_handle* txn)
{
	slev_input in;	
	slev_output out;	

	gen_slev_input(&in);

	m_slev_stmt.BindInput(0, SQL_SHORT, 2, &m_Warehouse);
	m_slev_stmt.BindInput(1, SQL_SHORT, 2, &in.d_id);
	m_slev_stmt.BindInput(2, SQL_LONG, 4, &in.threshold);
	
	m_slev_stmt.BindOutput(0, SQL_LONG, 4, &out.stock_count);

	return m_slev_stmt.Execute(diag, txn);
}

int v_printf(const char* text, ...)
{
	va_list ptr;
	va_start(ptr, text);

	LockGuard<Mutex> lock(&printfLock);

	int res = vprintf(text, ptr);
	va_end(ptr);
	return res;
}
