#ifndef __TPCC_H
#define __TPCC_H

#include <ibase.h>

#ifdef __cplusplus

#ifndef _WINDOWS

#include <pthread.h>

class Mutex {
private:
	pthread_mutex_t mlock;
public:
	Mutex() {
		if (pthread_mutex_init(&mlock, 0))
			throw("pthread_mutex_init");
	}
	~Mutex() {
		if (pthread_mutex_destroy(&mlock))
			throw("pthread_mutex_destroy");
	}
	void enter() {
		if (pthread_mutex_lock(&mlock))
			throw("pthread_mutex_lock");
	}
	void leave() {
		if (pthread_mutex_unlock(&mlock))
			throw("pthread_mutex_unlock");
	}
	void Lock() {
		enter();
	}
	void Unlock() {
		leave();
	}
};

typedef Mutex CRITICAL_SECTION;
inline void EnterCriticalSection(CRITICAL_SECTION* mtx)
{
	mtx->enter();
}
inline void LeaveCriticalSection(CRITICAL_SECTION* mtx)
{
	mtx->leave();
}
#else
class Mutex
{
public:
	void Lock() {EnterCriticalSection(&m_sec);}
	void Unlock() {LeaveCriticalSection(&m_sec);}
	Mutex() {InitializeCriticalSection(&m_sec);}
	~Mutex() {DeleteCriticalSection(&m_sec);}
	CRITICAL_SECTION m_sec;
};
#endif

extern "C" {
#endif

#define DIST_PER_WARE 10

#define MAX_WAREHOUSE_NAME_LEN 10
#define MAX_DISTRICT_NAME_LEN 10
#define MAX_CUSTOMER_DATA_LEN 500
#define MAX_HISTORY_DATA_LEN 24

#define MAX_STREET_LEN 20
#define MAX_CITY_LEN 20
#define MAX_STATE_LEN 2
#define MAX_ZIP_LEN 9
#define MAX_PHONE_LEN 16

#define MAX_LAST_NAME_LEN 16
#define MAX_FIRST_NAME_LEN 16
#define MAX_MIDDLE_NAME_LEN 2

#define CREDIT_LEN 2
#define MAX_ITEM_NAME_LEN 24
#define MAX_ITEM_DATA_LEN 50
#define MAX_STOCK_DATA_LEN 50
#define MAX_ORDER_ITEMS 20
#define MAX_DIST_LEN 24

#define MAX_BRAND_LEN 1
//#define MAX_OL_NEW_ORDER_ITEMS 15
//#define MAX_OL_ORDER_STATUS_ITEMS 15
#define MAX_STATUS_LEN 25

// basic data types

#ifndef _WINDOWS
	typedef unsigned long UINT64;
	typedef char BOOL;
#endif

typedef ISC_TIMESTAMP datetime_t;
typedef ISC_INT64 amount_t;

typedef ISC_USHORT w_id_t;
typedef ISC_USHORT d_id_t;
typedef ISC_ULONG c_id_t;
typedef ISC_ULONG o_id_t;
//typedef ISC_ULONG i_id_t;

typedef ISC_ULONG h_id_t;
typedef ISC_ULONG i_id_t;
typedef ISC_ULONG im_id_t;

typedef ISC_ULONG percent_t;
typedef ISC_USHORT quantity_t;
typedef ISC_USHORT carrier_id_t;

typedef ISC_USHORT bool_t;
#define FALSE (0)
#define TRUE (~0)

#define PERCENT_SCALE   10000   /* for percent */
#define MONEY_SCALE     100     /* for amount, money, price, */

// diagnostics definititions

#define OK 0
#define FAILURE 1
#define INVALID_ITEM_NUMBER 2

#define MAX_DIAG_MSG_LEN 512

typedef struct
{
	int sql_code;
	char message[MAX_DIAG_MSG_LEN + 1];
} Diag;

// random number function prototypes

void seed(long val);
double drand(void);
long RandomNumber(long lower, long upper);
long NURand(int iConst, long x, long y, long C);

// text function prototypes

void MakeAddress(char *street_1, char *street_2, 
	char *city, char *state, char *zip);

void LastName(int num, char *name);

int MakeAlphaString(int x, int y, int z, char *str);

int MakeOriginalAlphaString(int x, int y, int z, 
	char *str, int percent);

int MakeNumberString(int x, int y, int z, char *str);

int MakeZipNumberString(int x, int y, int z, char *str);

void InitString(char *str, int len);

// date-time function prototypes

void TimeInit(void);
ISC_ULONG TimeNow(void);

void current_timestamp(
	datetime_t* ts);

void zero_timestamp(
	datetime_t* ts);

// disgnostics prototypes

void diagnostics(
	Diag* diag,
	const ISC_STATUS* isc_status,
	int SQLCODE);

#ifdef __cplusplus
}
#endif

#endif
