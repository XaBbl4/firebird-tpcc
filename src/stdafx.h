#ifndef FIREBIRD_TPCC
#define FIREBIRD_TPCC

#define WIN32_LEAN_AND_MEAN

#ifndef _WINDOWS
#include <unistd.h>
#else
#include <windows.h>
#include <winbase.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <sys/timeb.h>
#include <sys/types.h>

#include <ibase.h>

#endif
