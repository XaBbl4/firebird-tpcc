#!/bin/bash
set -e

show_warning () {
	echo >&2 'Warning: missing environment variables DB_NAME'
	echo >&2 'All variables:'
	echo >&2 '- DB_HOST - Host with Firebird Server'
	echo >&2 '- DB_NAME - Path to the database'
	echo >&2 '- DB_USER - Username (default: sysdba)'
	echo >&2 '- DB_PASSWORD - Password (default: masterkey)'
	echo >&2 '- DB_SCALE - Scaled database (default: 0 - normal, 1 - tiny)'
	echo >&2 '- DB_WAREHOUSES - Number of Warehouses (default: 1)'
	echo >&2 '- DB_TERMINALS - Number of terminals (default: 1)'
	echo >&2 '- WARMUP_TIME - Warm-up duration, min (default: 30)'
	echo >&2 '- BENCHMARK_TIME - Test run duration, min (default: 150)'
	echo >&2 '- REPORT_INTERVAL - Report interval, sec (default: 30)'
}

if [ "$1" = 'create' ] || [ "$1" = 'load' ] || [ "$1" = 'start' ]; then
	if [ -z "$DB_NAME" ]; then
		show_warning
		exit 1
	fi

	if [ "$DB_HOST" ]; then
		DB_HOST="$DB_HOST:"
	fi
	if [ -z "$DB_USER" ]; then
		DB_USER="SYSDBA"
	fi
	if [ -z "$DB_PASSWORD" ]; then
		DB_PASSWORD="masterkey"
	fi
	if [ "$DB_SCALE" ]; then
		DB_SCALE="-c$DB_SCALE"
	fi
	if [ "$DB_WAREHOUSES" ]; then
		DB_WAREHOUSES="-W$DB_WAREHOUSES"
	fi
	if [ "$DB_TERMINALS" ]; then
		DB_TERMINALS="-T$DB_TERMINALS"
	fi
	if [ "$WARMUP_TIME" ]; then
		WARMUP_TIME="-r$WARMUP_TIME"
	fi
	if [ "$BENCHMARK_TIME" ]; then
		BENCHMARK_TIME="-R$BENCHMARK_TIME"
	fi
	if [ "$REPORT_INTERVAL" ]; then
		REPORT_INTERVAL="-i$REPORT_INTERVAL"
	fi
fi

if [ "$1" = 'create' ]; then

	echo 'Creating database'
	echo "create database '$DB_HOST$DB_NAME';" | /opt/RedDatabase/bin/isql -user $DB_USER -password $DB_PASSWORD -q

	echo 'Apply structure of database'
	/opt/RedDatabase/bin/isql -user $DB_USER -password $DB_PASSWORD $DB_HOST$DB_NAME -q -i /opt/firebird-tpcc/sql/database.sql

	exit 0

elif [ "$1" = 'load' ]; then

	echo 'Set async mode of database'
	/opt/RedDatabase/bin/gfix -user $DB_USER -password $DB_PASSWORD -write async $DB_HOST$DB_NAME

	echo 'Loading data to the database'
	/opt/firebird-tpcc/bin/load -U$DB_USER -P$DB_PASSWORD -D$DB_HOST$DB_NAME $DB_SCALE $DB_WAREHOUSES

	echo 'Apply index'
	/opt/RedDatabase/bin/isql -user $DB_USER -password $DB_PASSWORD $DB_HOST$DB_NAME -q -i /opt/firebird-tpcc/sql/index.sql

	echo 'Apply foreign'
	/opt/RedDatabase/bin/isql -user $DB_USER -password $DB_PASSWORD $DB_HOST$DB_NAME -q -i /opt/firebird-tpcc/sql/foreign.sql

	echo 'Apply procedures'
	/opt/RedDatabase/bin/isql -user $DB_USER -password $DB_PASSWORD $DB_HOST$DB_NAME -q -i /opt/firebird-tpcc/sql/proc.sql

	echo 'Set sync mode of database'
	/opt/RedDatabase/bin/gfix -user $DB_USER -password $DB_PASSWORD -write sync $DB_HOST$DB_NAME

	exit 0

elif [ "$1" = 'start' ]; then

	# shift

	echo 'Start TPC-C'
	exec /opt/firebird-tpcc/bin/tpcc -U$DB_USER -P$DB_PASSWORD -D$DB_HOST$DB_NAME -a1 -b1 $DB_SCALE $DB_WAREHOUSES $DB_TERMINALS $WARMUP_TIME $BENCHMARK_TIME $REPORT_INTERVAL

fi

exec "$@"