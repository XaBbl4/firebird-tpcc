set term @@ ;

create procedure neword1 
(
	w_id smallint,
	in_d_id smallint,
	in_c_id int,
	in_o_ol_cnt smallint,
	in_o_all_local smallint
)
returns 
(
	out_o_id int,
	out_c_last varchar(16),
	out_c_credit char(2),
	out_c_discount numeric(4,4),
	out_w_tax numeric(4,4),
	out_d_tax numeric(4,4),
	out_o_entry_d timestamp
)
as
begin

	out_o_entry_d = CURRENT_TIMESTAMP;

	SELECT GEN_ID(ORDER_ID, 1), d_tax 
		FROM district
		WHERE d_id = :in_d_id AND d_w_id = :w_id
		INTO :out_o_id, :out_d_tax;
		
	SELECT c_discount, c_last, c_credit, w_tax
		FROM customer, warehouse
		WHERE w_id = :w_id AND c_w_id = w_id AND
			c_d_id = :in_d_id AND c_id = :in_c_id
		INTO :out_c_discount, :out_c_last, :out_c_credit, :out_w_tax;

	INSERT INTO ORDERS (o_id, o_d_id, o_w_id, o_c_id,
			o_entry_d, o_ol_cnt, o_all_local, o_carrier_id)
		VALUES (:out_o_id, :in_d_id, :w_id, :in_c_id,
			:out_o_entry_d, :in_o_ol_cnt, :in_o_all_local, NULL);

	INSERT INTO NEW_ORDER (no_o_id, no_d_id, no_w_id)
		VALUES (:out_o_id, :in_d_id, :w_id);

end @@

create procedure neword2 
(
	w_id smallint,
	d_id smallint,
	o_id int,
	ol_number smallint,
	in_ol_i_id int,
	in_ol_supply_w_id smallint,
	in_ol_quantity numeric(9,0)
)
returns
(
	out_item_not_found smallint,
	out_i_name varchar(24),
	out_s_quantity numeric(4,0),
	out_brand_generic char(1),
	out_i_price numeric(9,2),
	out_ol_amount numeric(18,2)
)
as
	declare variable remote smallint;
	declare variable s_dist_0 varchar(24);
	declare variable s_dist_1 varchar(24);
	declare variable s_dist_2 varchar(24);
	declare variable s_dist_3 varchar(24);
	declare variable s_dist_4 varchar(24);
	declare variable s_dist_5 varchar(24);
	declare variable s_dist_6 varchar(24);
	declare variable s_dist_7 varchar(24);
	declare variable s_dist_8 varchar(24);
	declare variable s_dist_9 varchar(24);
	declare variable s_dist varchar(24);
	declare variable i_data varchar(50);
	declare variable s_data varchar(50);
begin

	if (in_ol_supply_w_id != w_id) then
		remote = 1;
	else
		remote = 0;

	out_item_not_found = 1;

	SELECT i_price, i_name, i_data, 0
	FROM item
	WHERE i_id = :in_ol_i_id
	INTO :out_i_price, :out_i_name, :i_data, :out_item_not_found;

	if (out_item_not_found = 0) then
	begin

		SELECT s_quantity, s_data,
			s_dist_01, s_dist_02, s_dist_03, s_dist_04, s_dist_05,
			s_dist_06, s_dist_07, s_dist_08, s_dist_09, s_dist_10
		FROM stock
		WHERE s_i_id = :in_ol_i_id AND s_w_id = :in_ol_supply_w_id
		INTO :out_s_quantity, :s_data,
			:s_dist_0, :s_dist_1, :s_dist_2, :s_dist_3, :s_dist_4,
			:s_dist_5, :s_dist_6, :s_dist_7, :s_dist_8, :s_dist_9;

        if ((i_data containing 'original') and
            (s_data containing 'original')) then
            out_brand_generic = 'B';
        else
            out_brand_generic = 'G';

		if (out_s_quantity >= in_ol_quantity + 10) then
			out_s_quantity = out_s_quantity - in_ol_quantity;
		else
			out_s_quantity = out_s_quantity - in_ol_quantity + 91;

		UPDATE stock 
		SET 
			s_quantity = :out_s_quantity,
			s_ytd = s_ytd + :in_ol_quantity,
			s_order_cnt = s_order_cnt + 1,
			s_remote_cnt = s_remote_cnt + :remote
		WHERE s_i_id = :in_ol_i_id AND s_w_id = :in_ol_supply_w_id;

		out_ol_amount = in_ol_quantity * out_i_price; /* integer * scaled */

		if (d_id = 1) then
			s_dist = s_dist_0;
		else
		if (d_id = 2) then
			s_dist = s_dist_1;
		else
		if (d_id = 3) then
			s_dist = s_dist_2;
		else
		if (d_id = 4) then
			s_dist = s_dist_3;
		else
		if (d_id = 5) then
			s_dist = s_dist_4;
		else
		if (d_id = 6) then
			s_dist = s_dist_5;
		else
		if (d_id = 7) then
			s_dist = s_dist_6;
		else
		if (d_id = 8) then
			s_dist = s_dist_7;
		else
		if (d_id = 9) then
			s_dist = s_dist_8;
		else
		if (d_id = 10) then
			s_dist = s_dist_9;

		INSERT INTO order_line (ol_o_id, ol_d_id, ol_w_id, ol_number,
			ol_i_id, ol_supply_w_id, ol_quantity, ol_amount, ol_dist_info,
			ol_delivery_d)
		VALUES (:o_id, :d_id, :w_id, :ol_number,
			:in_ol_i_id, :in_ol_supply_w_id,
			:in_ol_quantity, :out_ol_amount, :s_dist, NULL);
	end
end @@

create procedure payment
(
	w_id smallint,
	in_d_id smallint,
	in_c_w_id smallint,
	in_c_d_id smallint,
	in_c_id int,
	in_c_last varchar(16),
	in_h_amount numeric(18,2)
)
returns
(
	out_h_date timestamp,
	out_w_name varchar(10),
	out_w_street_1 varchar(20),
	out_w_street_2 varchar(20),
	out_w_city varchar(20),
	out_w_state char(2),
	out_w_zip varchar(9),
	out_d_name varchar(10),
	out_d_street_1 varchar(20),
	out_d_street_2 varchar(20),
	out_d_city varchar(20),
	out_d_state char(2),
	out_d_zip varchar(9),
	out_c_id int,
	out_c_first varchar(16),
	out_c_middle varchar(2),
	out_c_last varchar(16),
	out_c_street_1 varchar(20),
	out_c_street_2 varchar(20),
	out_c_city varchar(20),
	out_c_state varchar(2),
	out_c_zip varchar(9),
	out_c_phone varchar(16),
	out_c_since timestamp,
	out_c_credit varchar(2),
	out_c_credit_lim numeric(18,2),
	out_c_discount numeric(4,4),
	out_c_balance numeric(18,2),
	out_c_data varchar(500)
)
as
	declare variable namecnt int;
	declare variable i int;

	declare variable c_id int;
	declare variable c_first varchar(16);
	declare variable c_middle varchar(2);
	declare variable c_street_1 varchar(20);
	declare variable c_street_2 varchar(20);
	declare variable c_city varchar(20);
	declare variable c_state varchar(2);
	declare variable c_zip varchar(9);
	declare variable c_phone varchar(16);
	declare variable c_since timestamp;
	declare variable c_credit varchar(2);
	declare variable c_credit_lim numeric(18,2);
	declare variable c_discount numeric(4,4);
	declare variable c_balance numeric(18,2);
begin

	out_h_date = CURRENT_TIMESTAMP;

	UPDATE warehouse 
	SET w_ytd = w_ytd + :in_h_amount
	WHERE w_id = :w_id;

	SELECT w_street_1, w_street_2, w_city, w_state, w_zip, w_name
	FROM warehouse
	WHERE w_id = :w_id
	INTO :out_w_street_1, :out_w_street_2, :out_w_city, :out_w_state, :out_w_zip, :out_w_name;

	UPDATE district 
	SET d_ytd = d_ytd + :in_h_amount
	WHERE d_w_id = :w_id AND d_id = :in_d_id;

	SELECT d_street_1, d_street_2, d_city, d_state, d_zip, d_name
	FROM district
	WHERE d_w_id=:w_id AND d_id=:in_d_id
	INTO :out_d_street_1, :out_d_street_2, :out_d_city, :out_d_state, :out_d_zip, :out_d_name;
	
    if (in_c_id = 0) then
    begin

		SELECT COUNT(c_id)
		FROM customer
		WHERE c_last = :in_c_last AND c_d_id = :in_c_d_id AND c_w_id = :in_c_w_id
		INTO :namecnt;

		if (:namecnt / 2 * 2 != :namecnt) then
			namecnt = namecnt - 1;

		i = 0;
	
		FOR
		SELECT c_first, c_middle, c_id,
			c_street_1, c_street_2, c_city, c_state, c_zip,
			c_phone, c_credit, c_credit_lim,
			c_discount, c_balance, c_since
		FROM customer
		WHERE c_w_id=:in_c_w_id AND c_d_id=:in_c_d_id AND c_last=:in_c_last
		ORDER BY c_first
		INTO :c_first, :c_middle, :c_id,
			:c_street_1, :c_street_2, :c_city, :c_state, :c_zip,
			:c_phone, :c_credit, :c_credit_lim,
			:c_discount, :c_balance, :c_since
		DO 
		BEGIN

			if (:namecnt = i * 2) then
			begin
				out_c_first = :c_first;
				out_c_middle = :c_middle;
				out_c_id = :c_id;
				out_c_street_1 = :c_street_1;
				out_c_street_2 = :c_street_2;
				out_c_city = :c_city;
				out_c_state = :c_state;
				out_c_zip = :c_zip;
				out_c_phone = :c_phone;
				out_c_credit = :c_credit;
				out_c_credit_lim = :c_credit_lim;
				out_c_discount = :c_discount;
				out_c_balance = :c_balance;
				out_c_since = :c_since;
			end

			i = i + 1;
		END
	END
    else
    BEGIN
		SELECT c_first, c_middle, c_last,
			c_street_1, c_street_2, c_city, c_state, c_zip,
			c_phone, c_credit, c_credit_lim,
			c_discount, c_balance, c_since
		FROM customer
		WHERE c_w_id=:in_c_w_id AND c_d_id=:in_c_d_id AND c_id=:in_c_id
		INTO :out_c_first, :out_c_middle, :out_c_last,
			:out_c_street_1, :out_c_street_2, :out_c_city, :out_c_state, :out_c_zip,
			:out_c_phone, :out_c_credit, :out_c_credit_lim,
			:out_c_discount, :out_c_balance, :out_c_since;

		out_c_id = in_c_id;
    END
	
    out_c_balance = out_c_balance + in_h_amount;

    if (out_c_credit = 'BC') then
    begin

		FOR 
		SELECT c_data
		FROM customer
		WHERE c_w_id = :in_c_w_id AND c_d_id = :in_c_d_id AND c_id = :out_c_id
		INTO :out_c_data 
		AS CURSOR c
		DO BEGIN

			out_c_data = SUBSTRING(:out_c_id 
				|| ' ' || :in_c_d_id 
				|| ' ' || :in_c_w_id
				|| ' ' || :in_d_id
				|| ' ' || :w_id
				|| ' ' || :in_h_amount
				|| ' ' || :out_c_data FROM 1 FOR 500);

			UPDATE customer
			SET 
				c_balance = :out_c_balance, 
				c_data = :out_c_data
			WHERE CURRENT OF c;

		END

		out_c_data = SUBSTRING(out_c_data FROM 1 FOR 200);

		INSERT INTO history (h_c_d_id, h_c_w_id, h_c_id, h_d_id,
			h_w_id, h_date, h_amount, h_data)
		VALUES (:in_c_d_id, :in_c_w_id, :out_c_id, :in_d_id,
			:w_id, :out_h_date, :in_h_amount, :out_w_name || ' ' || :out_d_name);

    end
    else
    begin

		UPDATE customer 
		SET c_balance = :out_c_balance
		WHERE c_w_id = :in_c_w_id AND c_d_id = :in_c_d_id AND c_id = :out_c_id;

		out_c_data = '';

    end
end @@

create procedure ostat1
(
	in_w_id smallint,
	in_d_id smallint,
	in_c_id int,
	in_c_last varchar(16)
)
returns
(
	out_c_id int,
	out_c_first varchar(16),
	out_c_middle varchar(2),
	out_c_last varchar(16),
	out_c_balance numeric(18,2),
	out_o_id int,
	out_o_entry_d timestamp,
	out_o_carrier_id smallint
)
as
	declare variable namecnt int;
	declare variable i int;
	declare variable c_id int;
	declare variable c_first varchar(16);
	declare variable c_middle varchar(2);
	declare variable c_balance numeric(18,2);
begin

    if (in_c_id = 0) then
    begin

		SELECT COUNT(c_id)
		FROM customer
		WHERE c_last = :in_c_last AND c_d_id = :in_d_id AND c_w_id = :in_w_id
		INTO :namecnt;

		if (:namecnt / 2 * 2 != :namecnt) then
			namecnt = namecnt - 1;

		i = 0;
	
		FOR
		SELECT c_balance, c_first, c_middle, c_id
		FROM customer
		WHERE c_w_id=:in_w_id AND c_id=:in_d_id AND c_last=:in_c_last
		ORDER BY c_first
		INTO :out_c_balance, :out_c_first, :out_c_middle, :out_c_id
		DO 
		BEGIN

			if (:namecnt = i * 2) then
			begin
				out_c_balance = :c_balance;
				out_c_first = :c_first;
				out_c_middle = :c_middle;
				out_c_id = :c_id;
			end

			i = i + 1;
		END
	END
    else
    BEGIN
		SELECT c_balance, c_first, c_middle, c_last
		FROM customer
		WHERE c_id=:in_c_id AND c_d_id=:in_d_id AND c_w_id=:in_w_id
		INTO :out_c_balance, :out_c_first, :out_c_middle, :out_c_last;

		out_c_id = in_c_id;
	END

/**
	-- TODO using index when optimizer is fixed

    SELECT o_id, o_carrier_id, o_entry_d
    FROM orders
    WHERE o_w_id = :in_w_id AND o_d_id=:in_d_id AND o_id = (
        SELECT MAX(o_id)
        FROM orders
        WHERE o_w_id = :in_w_id AND o_d_id=:in_d_id)
**/
    SELECT o_id, o_carrier_id, o_entry_d
    FROM orders
    WHERE o_w_id = :in_w_id AND o_d_id=:in_d_id
    ORDER BY o_w_id DESC, o_d_id DESC, o_id DESC
    ROWS 1
    INTO :out_o_id, :out_o_carrier_id, :out_o_entry_d;
END @@

create procedure delivery
(
	w_id smallint,
	in_o_carrier_id smallint
)
as
	declare variable datetime timestamp;
	declare variable d_id smallint;
	declare variable no_o_id int;
	declare variable c_id int;
	declare variable ol_total numeric(18,2);
begin

	datetime = CURRENT_TIME;
	d_id = 1;
	
	while (d_id <= 10) do
	begin

		no_o_id = NULL;

		SELECT MIN(no_o_id) 
		FROM new_order
		WHERE no_d_id = :d_id AND no_w_id = :w_id
		INTO :no_o_id;

		if (no_o_id IS NOT NULL) then
		begin

			DELETE FROM new_order 
			WHERE no_d_id = :d_id AND no_w_id = :w_id AND no_o_id = :no_o_id;

			SELECT o_c_id 
			FROM orders
			WHERE o_id = :no_o_id AND o_d_id = :d_id AND o_w_id = :w_id 
			INTO :c_id;

			UPDATE orders 
			SET o_carrier_id = :in_o_carrier_id
			WHERE o_id = :no_o_id AND o_d_id = :d_id AND o_w_id = :w_id;

			UPDATE order_line 
			SET ol_delivery_d = :datetime
			WHERE ol_o_id = :no_o_id AND ol_d_id = :d_id AND ol_w_id = :w_id;

			SELECT SUM(ol_amount)
			FROM order_line
			WHERE ol_o_id = :no_o_id AND ol_d_id = :d_id AND ol_w_id = :w_id 
			INTO :ol_total;

            if (ol_total IS NOT NULL AND ol_total <> 0) then
				UPDATE customer 
				SET c_balance = c_balance + :ol_total
				WHERE c_id = :c_id AND c_d_id = :d_id AND c_w_id = :w_id;

        end
		d_id = d_id + 1;
	end
end @@

create procedure slev
(
	w_id smallint,
	in_d_id smallint,                /* must be fixed for a terminal */
	in_threshold int
)
returns
(
	out_stock_count int
)
as
	declare variable o_id int;
begin

	SELECT d_next_o_id 
	FROM district
	WHERE d_w_id=:w_id AND d_id=:in_d_id
	INTO :o_id;

	SELECT COUNT(DISTINCT(s_i_id)) 
	FROM order_line, stock
	WHERE ol_w_id=:w_id AND
		ol_d_id=:in_d_id AND ol_o_id<:o_id AND
		ol_o_id>=:o_id-20 AND s_w_id=:w_id AND
		s_i_id=ol_i_id AND s_quantity<:in_threshold
	INTO :out_stock_count;

end @@

set term ; @@
