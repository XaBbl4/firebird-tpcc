alter table warehouse
	add constraint warehouse_pk primary key(w_id);

alter table district 
	add constraint district_pk primary key(d_w_id, d_id);

alter table customer 
	add constraint customer_pk primary key (c_w_id, c_d_id, c_id);

alter table orders
	add constraint orders_pk primary key(o_w_id, o_d_id, o_id)
	using desc index orders_pk;

alter table new_order
	add constraint new_order_pk primary key (no_w_id, no_d_id, no_o_id);

alter table item
	add constraint item_pk primary key(i_id);

alter table stock
	add constraint stock_pk primary key(s_w_id, s_i_id);

alter table order_line
	add constraint order_line_pk primary key(ol_w_id, ol_d_id, ol_o_id, ol_number);

create index customer_last on customer (c_last);
