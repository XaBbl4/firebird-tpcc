create generator order_id;
set generator order_id to 1000000;

create domain tp_boolean smallint; /* check numeric(1) */
create domain datetime	timestamp;

create domain name	varchar(10);
create domain street	varchar(20);
create domain city	varchar(20);
create domain state	char(2);
create domain zip	char(9);
create domain first_name varchar(16);
create domain middle_name char(2);
create domain last_name varchar(16);
create domain phone char(16);
create domain credit char(2); /* "gc"=good, "bc"=bad */

create domain percent	numeric(4,4);
create domain money	numeric(18,2); /* 12,2 */
create domain amount	numeric(9,2); /* 6,2 */
create domain price numeric(9,2); /* 5 */
create domain quantity numeric(4); /* 2 */

create domain w_id	smallint;
create domain d_id smallint; /* 10 are populated per warehouse */
create domain c_id integer;
create domain o_id integer; /* 10 000 000 */
create domain ol_number	smallint;
create domain h_id	integer;
create domain i_id	integer; /* 200,000 unique ids 100,000 items are populated */
create domain im_id	integer; /* 200,000 unique ids image id associated to item */
create domain carrier_id smallint; /* 10 unique ids */
create domain dist char(24);

create table warehouse
(
	w_id	w_id	not null,
	w_name	name	not null,
	w_street_1	street	not null,
	w_street_2	street	not null,
	w_city	city	not null,
	w_state	state	not null,
	w_zip	zip not null,
	w_tax	percent	not null,
	w_ytd	money	not null
);

create table district
(
	d_id	d_id	not null,
	d_w_id	w_id	not null,
	d_name	name	not null,
	d_street_1	street	not null,
	d_street_2	street	not null,
	d_city	city	not null,
	d_state	state	not null,
	d_zip	zip	not null,
	d_tax	percent	not null,
	d_ytd	money	not null,
	d_next_o_id	o_id	not null
);

create table customer
(
	c_id	c_id	not null,
	c_d_id	d_id	not null,
	c_w_id	w_id	not null,
	c_first	first_name	not null,
	c_middle	middle_name	not null,
	c_last	last_name	not null,
	c_street_1	street	not null,
	c_street_2	street	not null,
	c_city	city	not null,
	c_state	state	not null,
	c_zip	zip	not null,
	c_phone	phone	not null,
	c_since datetime	not null,
	c_credit	credit	not null,
	c_credit_lim	money	not null,
	c_discount	percent	not null,
	c_balance	money	not null,
	c_ytd_payment	money	not null,
	c_payment_cnt	smallint	not null,
	c_delivery_cnt	smallint not null,
	c_data	varchar(500)	not null
);

create table history
(
	h_c_id	h_id	not null,
	h_c_d_id	d_id	not null,
	h_c_w_id	w_id	not null,
	h_d_id	d_id	not null,
	h_w_id	w_id	not null,
	h_date	datetime	not null,
	h_amount	amount	not null,
	h_data	varchar(24)	not null
);

create table orders
(
	o_id	o_id	not null,
	o_d_id	d_id	not null,
	o_w_id	w_id	not null,
	o_c_id	c_id	not null,
	o_entry_d	datetime	not null,
	o_carrier_id	carrier_id, /* null */
	o_ol_cnt	ol_number	not null, /* from 5 to 15 count of order-lines */
	o_all_local tp_boolean	not null
);

create table new_order
(
	no_o_id	o_id	not null,
	no_d_id	d_id	not null,
	no_w_id	w_id	not null
);

create table new_log
(
	no_o_id	o_id	not null,
	no_d_id	d_id	not null,
	no_w_id	w_id	not null,
	step int		not null
);

create table item
(
	i_id	i_id	not null,
	i_im_id	im_id	not null,
	i_name	varchar(24)	not null,
	i_price	price	not null,
	i_data	varchar(50)	not null
);

create table stock
(
	s_i_id	i_id	not null,
	s_w_id	w_id	not null,
	s_quantity	quantity	not null,
	s_dist_01	dist	not null,
	s_dist_02	dist	not null,
	s_dist_03	dist	not null,
	s_dist_04	dist	not null,
	s_dist_05	dist	not null,
	s_dist_06	dist	not null,
	s_dist_07	dist	not null,
	s_dist_08	dist	not null,
	s_dist_09	dist	not null,
	s_dist_10	dist	not null,
	s_ytd	integer	not null,
	s_order_cnt	integer	not null,
	s_remote_cnt	integer not null,
	s_data	varchar(50)	not null
);

create table order_line
(
	ol_o_id	o_id	not null,
	ol_d_id	d_id	not null,
	ol_w_id	w_id	not null,
	ol_number	ol_number	not null,
	ol_i_id	i_id	not null,
	ol_supply_w_id	w_id	not null,
	ol_delivery_d datetime, /* null */
	ol_quantity	quantity	not null,
	ol_amount	amount	not null,
	ol_dist_info	dist	not null
);

