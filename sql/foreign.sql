alter table district 
	add constraint fk_d_w foreign key (d_w_id) references warehouse;

alter table customer 
	add constraint fk_c_d foreign key (c_w_id, c_d_id) references district;

alter table history
	add constraint fk_h_c foreign key (h_c_w_id, h_c_d_id, h_c_id) references customer;

alter table history
	add constraint fk_h_d foreign key (h_w_id, h_d_id) references district;

alter table orders
	add constraint fk_o_c foreign key (o_w_id, o_d_id, o_c_id) references customer;

alter table new_order
	add constraint fk_no_o foreign key (no_w_id, no_d_id, no_o_id) references orders;

alter table stock
	add constraint fk_s_w foreign key (s_w_id) references warehouse;

alter table stock
	add constraint fk_s_i foreign key (s_i_id) references item;

alter table order_line
	add constraint fk_ol_o foreign key (ol_w_id, ol_d_id, ol_o_id) references orders;

alter table order_line
	add constraint fk_ol_s foreign key (ol_supply_w_id, ol_i_id) references stock;
