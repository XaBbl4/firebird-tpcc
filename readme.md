## firebird-tpcc

This is [firebird-tpcc](https://bitbucket.org/XaBbl4/firebird-tpcc/src) in a docker image.

### How to use

#### Preparing database (create database)

```bash
# Local database (without TCP/IP protocol)
docker run -it --rm -v bases:/bases -e DB_NAME=/bases/tpcc.fdb xabbl4/firebird-tpcc create
```
or
```bash
# Network level
docker run -it --rm --link=xabbl4/red-database:rdb -e DB_HOST=rdb/3050 -e DB_NAME=/bases/tpcc.fdb xabbl4/firebird-tpcc create
```

#### Load test data

```bash
docker run -it --rm --link=xabbl4/red-database:rdb -e DB_HOST=rdb/3050 -e DB_NAME=/bases/tpcc.fdb -e DB_SCALE=1 -e DB_WAREHOUSES=10 xabbl4/firebird-tpcc load
```

#### Start test data

```bash
docker run -it --rm --link=xabbl4/red-database:rdb -e DB_HOST=rdb/3050 -e DB_NAME=/bases/tpcc.fdb -e DB_SCALE=1 -e DB_WAREHOUSES=10 -e DB_TERMINALS=10 -e WARMUP_TIME=2 -e BENCHMARK_TIME=10 -e REPORT_INTERVAL=60 xabbl4/firebird-tpcc start
```